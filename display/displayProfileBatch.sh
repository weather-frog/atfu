#!/bin/bash
#############
#default profile
profileID=q423  # default
profileDIR=../data/profiles/

# look fur user input
while [ "$#" -gt 0 ]; do
    key=$1
    case $1 in
    -p)
    shift
    IDFile=$1
    ;;
    -o) # define additional output format
    shift
    format=$1
    ;;
    *)
    echo "Usage: displayProfile -p IDFile -o format"
    echo "where IDFile is a file containing a list of profileIDs (one per line)"
    echo "profile files are expected as: $profileDIR/profile_{profileID}.txt"
    echo "format can be png (200dpi) or pdf"
    exit
    ;;
    esac
    shift
done

lines=`cat $IDFile`
for profileID in $lines; do 

sourcef="${profileDIR}/profile_${profileID}.txt"
echo "Processing $sourcef"

out="$profileID.eps"

# define print size
gmt gmtset PS_MEDIA=Custom_600x600

#create reference frame
frame='-JX17c/17c'
gmt psbasemap $frame -R0/600/0/600 -B+t"Atmospheric profile $profileID" -P -K > $out


# Add text parts
#gmt pstext -R -J -K << EOF >> $out
#300 500 20p 0 c Temperature profile
#EOF

# plot pressure against temperature
awk '{ print $2 ";" $1 }' $sourcef > tmpxy
xmin=180
xmax=310
ymin=0
ymax=1100
range="-R${xmin}/${xmax}/${ymin}/${ymax}"
anno='-B10:Temperature(line):/100:Pressure:WSe'
pen='-W1p'
frame='-JX17c/-16c'
# Note: the "-16c" is to reverse the y-axis

gmt psxy -h6 tmpxy $range $pen $anno $frame -Y-0.5c -P -O -K >> $out

# plot pressure against water-vapour
awk '{ print $3 ";" $1 }' $sourcef > tmpxy
xmin=0
xmax=0.025
range="-R${xmin}/${xmax}/${ymin}/${ymax}"
pen='-W1p,black,-'
anno='-B0.005:Water-vapour(dashes):/100:Pressure:N'
gmt psxy -h6 tmpxy $range $pen $anno $frame -P -O >> $out

if [ -n "$format" ] ; then
    case $format in
        png)
        gmtformat="-Tg -E200"
        ;;
        pdf)
        gmtformat="-Tf"
        gmt psconvert $profileID.eps -A -Tf
    esac
    echo "converting output to ${profileID}.$format"
    gmt psconvert $profileID.eps -A0.5c $gmtformat  
fi

done

rm tmpxz

