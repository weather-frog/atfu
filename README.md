# ATFU

Atmospheric Transformation Functions and Utilities

The aim of this project is to provide a toolbox for common atmospheric conversions / transformations. In addition, it provides methods to calculate atmospheric instability indices such as KI, LI or CAPE.

The following classes are available:

* airGlobal : Class to store global parameters like physical constants

* airParcel : Parcel of air defined by its pressure, temperature and water-vapour attributes and associated methods for conversions and transormations such as:  
  relative or specific humidity  
  dew point temperature  
  virtual temperature  
  water saturation presure  
  lifting condensation level  

* airProfile : Atmospheric profile (currently: pressure, temperature, water vapour, ozone and C02 mass mixing ratios) with the following methods available:
  reading data from file (constructor)  
  vertical interpolation  
  vertical integration of components  
  pressure to altitude conversion  

* airInstability : Class to calculate various instability indices for a given atmospheric profile

* airInstabilityBenchmark : Class to test the performances of airInstability