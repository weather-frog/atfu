/* 
   airGlobals.class
   
   Copyright 2019 Olivier Samain

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package atfu;

/**
 * Holds global variables used by the atfu package 
 */
public class airGlobals {    
    /** PI mathematical constant */
    public static double PI=3.14159265359;
    
    /** Default missing value */
    public static short MISSING=32767;
    
    /** Water molar mass (g/mol) */
    public static double MH2O=18.01534;
    
    /** Dry air molar mass (g/mol) */
    public static double ML=28.964;
    
    /** specific gas constant for dry air (J/kg K) */
    public static double RL=287.06;
    
    /** specific heat of dry air at constant pressure (J/kg/K) */
    public static double Cp=1005.71;
    
    /** water triple point. conversion from K to °C */
    public static double T0=273.15;
    
}
