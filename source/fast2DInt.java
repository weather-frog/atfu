/*
  This class implements a fast 2 dimensions interpolation that checks
  for invalid input grid points.
  
  The main requirement is speed, not accuracy or smothness.
  Therefore the interpolation method consists of 2 successive linear 
  interpolations (when the 4 surrounding points are valid) rather than
  true bilinear interpolation.
  
  For the case of 3 valid surrounding points, a plane fitting method is 
  used. For this purpose, the class implements a few simple matrix operations
  to avoid having to rely on external libraries.
  
  The entry point of the class is the GEN function. It is a pure, static function and can be 
  called directly as fast2DInt.GEN() without having to create an object instance.

*/
package atfu;

public class fast2DInt {
    
    // flag to enable trace. Recompile class to take effect.
    private static boolean trace=false;
    
    // flag to enable warning messages 
    private static boolean warning=true;
    
    // generic entry point for interpolation. It take as input the coordinates and field 
    // values for the 4 points surrounding the target XYout
    // **********************************************************************************
    public static double [] GEN(int Nvar,      // number of fields to interpolate
                            double[] Xin,      // first dimenstion coordinates (size=4)
                            double[] Yin,      // second dimenstion coordinates (size=4)
                            double[][] VarIn,  // field values (size=4xNvar)
                            byte[] mask,        // validity mask (1=valid) (size=4)
                            double[] XYout ) { // interpolation point coordinates (size=2)
        
        double[] VarOut = new double[Nvar];
        final double MISSING=airGlobals.MISSING;
        
        // check dimensions
        if (Xin.length!=4 || Yin.length!=4 || mask.length!=4 || VarIn.length!=4 || XYout.length!=2) {
            if (warning) System.out.println("One argument has wrong size");
            for (int n=0;n<Nvar;n++) {
                VarOut[n]=MISSING;
            }
            return VarOut;
        }
        else {
            for (int n=0;n<4;n++) {
                if (VarIn[n].length !=Nvar) {
                    if (warning) System.out.println("VarIn has the wrong number of columns");
                    for (int j=0;n<Nvar;n++) {
                        VarOut[j]=MISSING;
                    }
                    return VarOut;
                }
            }
        }
        
        // determine the valid points
        int nvalid=0;
        double[] Xvalid = new double[4];
        double[] Yvalid = new double[4];
        double[][] Zvalid = new double[4][Nvar];
        for (int i=0;i<4;i++) {
            if (mask[i]==1) {
                Xvalid[nvalid] = Xin[i];
                Yvalid[nvalid] = Yin[i];
                for (int n=0;n<Nvar;n++) {
                    Zvalid[nvalid][n] = VarIn[i][n];
                }
                nvalid++;
            }
        }
        
        if(nvalid==1) {
            if (trace) System.out.println("Nearest neighbour. Return data from the only available point");
            for (int n=0;n<Nvar;n++) {
                VarOut[n]=Zvalid[0][n];
            }
        }
        else if (nvalid==2) {
            if (trace) System.out.println("2-points interpolation");
            VarOut = L2P(Nvar, Xvalid, Yvalid, Zvalid, XYout,false);
        }
        else if (nvalid==3) {
            if (trace) System.out.println("3-points interpolation");
            VarOut = L3P(Nvar, Xvalid, Yvalid, Zvalid, XYout);
            
        }
        else if (nvalid==4) {
            if (trace) System.out.println("4-points interpolation");
            // first perform 2 points interpolations between P1 and P2, P3 and P4
            // then interpolate betweeen the last two results
            
            // first interpolation
            if (trace) System.out.println("first 2P interpolation");
            double[] tmpOut1 = L2P(Nvar, Xvalid, Yvalid, Zvalid, XYout, true);
            if (trace) System.out.format("    At: (%6.2f, %6.2f)  Vars=[%6.2f, %6.2f] %n", XYout[0],XYout[1],tmpOut1[0],tmpOut1[1] );
            if (trace) System.out.format("    projected point = (%6.2f, %6.2f) %n", tmpOut1[2],tmpOut1[3]);
            
            // second interpolation
            // first extract the last 2 valid points into new arrays
            double[] Xvalid2 = {Xvalid[2], Xvalid[3]};
            double[] Yvalid2 = {Yvalid[2], Yvalid[3]};
            double[][] Zvalid2 = new double[2][Nvar];
            for (int n=0;n<Nvar;n++) {
                Zvalid2[0][n] = Zvalid[2][n];
                Zvalid2[1][n] = Zvalid[3][n];
            }
            
            
            
            if (trace) System.out.println("second 2P interpolation");
            double[] tmpOut2 = L2P(Nvar, Xvalid2, Yvalid2, Zvalid2, XYout, true);
            if (trace) System.out.format("    At: (%6.2f, %6.2f)  Vars=[%6.2f, %6.2f] %n", XYout[0],XYout[1],tmpOut2[0],tmpOut2[1] );
            if (trace) System.out.format("    projected point = (%6.2f, %6.2f) %n", tmpOut2[2],tmpOut2[3]);
            
            // third interpolation
            Xvalid2[0] = tmpOut1[Nvar];
            Xvalid2[1] = tmpOut2[Nvar];
            Yvalid2[0] = tmpOut1[Nvar+1];
            Yvalid2[1] = tmpOut2[Nvar+1];
            for (int n=0;n<Nvar;n++) {
                Zvalid2[0][n] = tmpOut1[n];
                Zvalid2[1][n] = tmpOut2[n];
            }
            if (trace) System.out.println("third 2P interpolation");
            VarOut = L2P(Nvar, Xvalid2, Yvalid2, Zvalid2, XYout,false);
        }
        else {
            // there are no valid point
            if (trace) System.out.println("No valid points found");
            for (int n=0;n<Nvar;n++) {
                VarOut[n]=MISSING;
            }
        }
        
        return VarOut;
    }
    
    
    // Linear 2 Points interpolation
    // the size of the output array depends on the ProjectPoint flag
    // *************************************************************
    public static double[] L2P(int Nvar, double[] Xin, double[] Yin, double[][] Zin,
                               double[] XYout, boolean ProjectPoint) {
        
        int Nout=Nvar;
        if (ProjectPoint) Nout=Nvar+2;
 
        double[] VarOut = new double[Nout];
        double u1 = XYout[0]-Xin[0];
        double u2 = XYout[1]-Yin[0];
        double v1 = Xin[1]-Xin[0];
        double v2 = Yin[1]-Yin[0];
                
        double alpha = (u1*v1+u2*v2) / (v1*v1+v2*v2);
        //System.out.format("u= %5.2f %5.2f %n",u1,u2);
        //System.out.format("v= %5.2f %5.2f %n",v1,v2);
        //System.out.format("alpha= %5.2f %n",alpha);
        
        for (int i=0; i<Nvar;i++) {
            VarOut[i] = (1-alpha)*Zin[0][i] + alpha*Zin[1][i];
        }
        
        // coordinates of the projected point
        if (ProjectPoint) {
            VarOut[Nvar]   = Xin[0] + alpha*v1;
            VarOut[Nvar+1] = Yin[0] + alpha*v2;
        }
        return VarOut;
    }
    
    // 3 point interpolation
    // *********************
    public static double[] L3P(int Nvar, double[] Xin, double[] Yin, double[][] Zin, double[] XYout) {
        double[] VarOut = new double[Nvar];
        double[][] M = new double[3][3];
        
        // build the matrix to inverse
        if (trace) System.out.println("Matrix to invert:");
        for (int i=0;i<3;i++) {
            M[i][0] = Xin[i];
            M[i][1] = Yin[i];
            M[i][2] = 1.0;
            if (trace) System.out.format("%6.2f  %6.2f %6.2f %n",M[i][0], M[i][1], M[i][2]);
        }
        
        // test determinant
        double det = det3x3(M);
        if ( det == 0) {
            // we probable have 3 points on the same line.
            // in that case, default to a 2 points interpolation
            if (warning) {
                System.out.println("Null determinant. Points are colininear");
                System.out.println("Input Matrix was:");
                for (int i=0;i<3;i++) {
                    System.out.format("%6.2f  %6.2f %6.2f %n",M[i][0], M[i][1], M[i][2]);
                }
            }
            VarOut = L2P(Nvar, Xin, Yin, Zin, XYout, false);
        }
        else {
            // inverse matrix
            double[][] invM = invert(M, det);
            
            double[][] I = mult(M,invM);
            if (trace) {
                System.out.format("det = %6.2f, M*InvM:%n",det);
                for (int i=0;i<3;i++) {
                    System.out.format("%6.2f  %6.2f %6.2f %n",I[i][0], I[i][1], I[i][2]);
                }
            }    
            
            for (int n=0;n<Nvar;n++) {
                // calculate plane coefficients
                double[] V = {Zin[0][n], Zin[1][n],Zin[2][n]};
                double[] A = mult(invM,V);
                if (trace) System.out.format("A = [%8.3f, %8.3f, %8.3f ]%n", A[0], A[1], A[2]);
                
                // apply plane equation
                VarOut[n] = A[0]*XYout[0] + A[1]*XYout[1] + A[2]; 
            }
        }
        return VarOut;
    
    }
    
    // matrix multiplication with a vector
    private static double[] mult(double[][] M, double[] V) {
        double[] out = new double[M.length];
        // check size consistency
        if (M[0].length != V.length) return out;
        
        for (int i=0;i<M.length;i++) {
            out[i]=0;
            for (int j=0;j<V.length;j++) out[i]+=M[i][j]*V[j];
        }
        return out;
    }
    
    // matrix multiplication with another matrix
    private static double[][] mult(double[][] M, double[][] Q) {
        double[][] out = new double[M.length][Q[0].length];
        // check size consistency
        if (M[0].length != Q.length) return out;

        for (int i=0;i<M.length;i++) {
            for (int j=0;j<Q[0].length;j++) {
                out[i][j]=0;
                for (int k=0;k<Q.length;k++) out[i][j]+=M[i][k]*Q[k][j];
            }
        }
        return out;
    }
    
    // derterminant of a 2x2 matrix
    // the input matrix is provided in flat form (row major)
    private static double det2x2(double[] M) {
        if (M.length != 4) {
            // wring input size
            return 0;
        }
        else {
            return M[0]*M[3] - M[1]*M[2];
        }
    }
    
    // derterminant of a 3x3 matrix
    private static double det3x3(double[][] M) {
        if (M.length != 3) return 0;
        if (M[0].length!=3) return 0;
        
        // definition of sub-matrices
        double[] M1 = {M[1][1], M[1][2], M[2][1], M[2][2]};
        double[] M2 = {M[1][0], M[1][2], M[2][0], M[2][2]};
        double[] M3 = {M[1][0], M[1][1], M[2][0], M[2][1]};
        
        return M[0][0]*det2x2(M1) - M[0][1]*det2x2(M2) + M[0][2]*det2x2(M3);
    }
    
    // inverse of a 3x3 matrix
    private static double[][] invert(double[][] M, double det) {
        double[][] inv = new double[3][3];
        
        // sanity checks
        if (M.length != 3 || M[0].length!=3 || det==0) return inv;
        
        // sub-matrices
        double[] M1 = {M[1][1], M[1][2], M[2][1], M[2][2]};
        double[] M2 = {M[0][2], M[0][1], M[2][2], M[2][1]};
        double[] M3 = {M[0][1], M[0][2], M[1][1], M[1][2]};
        double[] M4 = {M[1][2], M[1][0], M[2][2], M[2][0]};
        double[] M5 = {M[0][0], M[0][2], M[2][0], M[2][2]};
        double[] M6 = {M[0][2], M[0][0], M[1][2], M[1][0]};
        double[] M7 = {M[1][0], M[1][1], M[2][0], M[2][1]};
        double[] M8 = {M[0][1], M[0][0], M[2][1], M[2][0]};
        double[] M9 = {M[0][0], M[0][1], M[1][0], M[1][1]};
        
        inv[0][0] = det2x2(M1)/det;
        inv[0][1] = det2x2(M2)/det;
        inv[0][2] = det2x2(M3)/det;
        inv[1][0] = det2x2(M4)/det;
        inv[1][1] = det2x2(M5)/det;
        inv[1][2] = det2x2(M6)/det;
        inv[2][0] = det2x2(M7)/det;
        inv[2][1] = det2x2(M8)/det;
        inv[2][2] = det2x2(M9)/det;
        
        return inv;
    }
    
    public static void main(String args[]) {
        
        System.out.println("Testing 2P interpolation");
        int Nvar=2;
        double[] X = {0.0,1.0};
        double[] Y = {0.0,2.0};
        
        double[][] Z = new double[2][Nvar];
        
        Z[0][0] = 0.0;
        Z[1][0] = 10.0;
        Z[0][1] = -2.0;
        Z[1][1] = -.0;
        
        System.out.format("Point 1: (%6.2f, %6.2f)  Vars=[%6.2f, %6.2f] %n", X[0],Y[0],Z[0][0],Z[0][1]);
        System.out.format("Point 2: (%6.2f, %6.2f)  Vars=[%6.2f, %6.2f] %n", X[1],Y[1],Z[1][0],Z[1][1]);
    
        double[] XYintp = {0.,0.};
        double[] L2Pout = fast2DInt.L2P(Nvar,X,Y,Z, XYintp,true);
        System.out.format("    At:  (%6.2f, %6.2f)  Vars=[%6.2f, %6.2f] %n", XYintp[0],XYintp[1],L2Pout[0],L2Pout[1] );
        System.out.format("    projected point = (%6.2f, %6.2f) %n", L2Pout[2],L2Pout[3]);
        
        XYintp[0] = 0.5;
        XYintp[1] = 0.5;
        L2Pout = fast2DInt.L2P(Nvar,X,Y,Z, XYintp,true);
        System.out.format("%n    At: (%6.2f, %6.2f)  Vars=[%6.2f, %6.2f] %n", XYintp[0],XYintp[1],L2Pout[0],L2Pout[1] );
        System.out.format("    projected point = (%6.2f, %6.2f) %n", L2Pout[2],L2Pout[3]);
        
        XYintp[0] = 0.5;
        XYintp[1] = 1;
        L2Pout = fast2DInt.L2P(Nvar,X,Y,Z, XYintp,true);
        System.out.format("%n    At: (%6.2f, %6.2f)  Vars=[%6.2f, %6.2f] %n", XYintp[0],XYintp[1],L2Pout[0],L2Pout[1] );
        System.out.format("    projected point = (%6.2f, %6.2f) %n", L2Pout[2],L2Pout[3]);
        
        XYintp[0] = 1.0;
        XYintp[1] = 1.0;
        L2Pout = fast2DInt.L2P(Nvar,X,Y,Z, XYintp,true);
        System.out.format("%n    At: (%6.2f, %6.2f)  Vars=[%6.2f, %6.2f] %n", XYintp[0],XYintp[1],L2Pout[0],L2Pout[1] );
        System.out.format("    projected point = (%6.2f, %6.2f) %n", L2Pout[2],L2Pout[3]);
        
        System.out.println("");
        System.out.println("Testing 3P interpolation");
        
        Nvar=1;
        
        double[][] Z3 = new double[3][Nvar];
        System.out.println("45° plane vs the xy plane, containing the x axis");
        double[] X3 = {0.0, 2.0, 0.0};
        double[] Y3 = {0.0, 0.0, 2.0};
        Z3[0][0] = 0.0;
        Z3[1][0] = 0.0;
        Z3[2][0] = 2.0;
        XYintp[0] = 0.5;
        XYintp[1] = 0.5;
        double[] L3Pout = fast2DInt.L3P(Nvar,X3,Y3,Z3, XYintp);
        System.out.format("    At: (%6.2f, %6.2f)  Vars=[%6.2f] %n", XYintp[0],XYintp[1],L3Pout[0] );
        
        System.out.println("45° plane vs the xy plane, containing the y axis");
        X3[0]=-1;
        Y3[0]=0;
        Z3[0][0]=1;
        
        X3[1]=0;
        Y3[1]=1;
        Z3[1][0]=0;
        
        X3[2]=1;
        Y3[2]=1;
        Z3[2][0]=-1;
        
        XYintp[0] = 0.5;
        XYintp[1] = 0.5;
        L3Pout = fast2DInt.L3P(Nvar,X3,Y3,Z3, XYintp);
        System.out.format("    At: (%6.2f, %6.2f)  Vars=[%6.2f] %n", XYintp[0],XYintp[1],L3Pout[0] );
        
        System.out.println("45° plane vs the xy plane, containing the (0,0) to (1,1) axis");
        X3[0]=0;
        Y3[0]=0;
        Z3[0][0]=0;
        
        X3[1]=1;
        Y3[1]=1;
        Z3[1][0]=0;
        
        X3[2]=-1;
        Y3[2]= 1;
        Z3[2][0]=1;
        
        XYintp[0] = 0.5;
        XYintp[1] = 0.5;
        L3Pout = fast2DInt.L3P(Nvar,X3,Y3,Z3, XYintp);
        System.out.format("    At: (%6.2f, %6.2f)  Vars=[%6.2f] %n", XYintp[0],XYintp[1],L3Pout[0] );
        XYintp[0] = 0.0;
        XYintp[1] = 0.5;
        L3Pout = fast2DInt.L3P(Nvar,X3,Y3,Z3, XYintp);
        System.out.format("    At: (%6.2f, %6.2f)  Vars=[%6.2f] %n", XYintp[0],XYintp[1],L3Pout[0] );
        
        System.out.println("");
        System.out.println("Testing 4P interpolation");
        Nvar=2;
        double[] X4 = new double[4];
        double[] Y4 = new double[4];
        byte[] mask = new byte[4];
        double[][] Z4 = new double[4][Nvar];
        
        X4[0]=0;
        Y4[0]=0;
        Z4[0][0]=0.0;
        Z4[0][1]=-0.5;
        mask[0]=1;
        
        X4[1]=1;
        Y4[1]=1;
        Z4[1][0]=0;
        Z4[1][1]=0;
        mask[1]=1;
        
        X4[2]=-1;
        Y4[2]= 1;
        Z4[2][0]=1;
        Z4[2][1]=1;
        mask[2]=1;
        
        X4[3]=0;
        Y4[3]= 1;
        Z4[3][0]=1;
        Z4[3][1]=2;
        mask[3]=1;
        
        XYintp[0] = 0.0;
        XYintp[1] = 0.5;
        double[] L4Pout = fast2DInt.GEN(Nvar,X4,Y4,Z4,mask,XYintp);
        System.out.format("    At: (%6.2f, %6.2f)  Vars=[%6.2f %6.2f] %n", XYintp[0],XYintp[1],L4Pout[0],L4Pout[1] );
        
    }
}
