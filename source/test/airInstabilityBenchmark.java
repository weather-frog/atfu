// The putpose of this class is to test the performances of the 
// instability indices. The benchmark consist of repeating the 
// calculations for the same profile many times.

/*
   Class to test the performances of airInstability
   
   It consists of simply looping using the same profiles
   The log level shall be set to WARNING of INFO at most
   
   Dependencies:
    - airGlobals
    - airParcel
    - airProfile
    - airInstability
   
   ----------------------------- 
   Copyright 2019 Olivier Samain

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
    
*/
package atfu.test;

import java.io.IOException;
import java.util.logging.Level;
import java.util.Arrays;
import atfu.*;


public class airInstabilityBenchmark {

    private int Nprofiles;
    private String inputFile;
    
    // constructor
    airInstabilityBenchmark(int N, String file) {
        Nprofiles = N;
        inputFile = file;
    }
    
    // execute benchmark
    public void run() throws IOException {
        
        // set logging
        String logFile="logs/airInstabilityBenchmark.log";
        airInstability.setLogging(true, "FINE");
        
        
        // loop on the number of executions
        for (int i=0; i<this.Nprofiles;i++) {
            
            
            airInstability INS = new airInstability(this.inputFile);
            
            // perform vertical integration twice
            String[] VarIds = {"W","O","C"};
            double[] VarBot = {airGlobals.MISSING,airGlobals.MISSING,airGlobals.MISSING};
            double[] TCOL=INS.TCOL(VarIds,1015.0,350.0,0.0,45.0,VarBot);
            TCOL=INS.TCOL(VarIds,1015.0,350.0,0.0,45.0,VarBot);
            
            double LI=INS.LI();
            double KI=INS.KI();
            double Ts = 300;
            double elev=0;
            double lat=45;
            double[] LPW=INS.LPW(elev,lat);
            double[] MB = INS.MB();
            double DThe = INS.DTHe(Arrays.copyOfRange(MB,1,MB.length));
            
            // randomize the start parcel. This only affects the first CAPE method.
            double T0=295 + Math.random()*10 -5;
            double W0=0.02 + (Math.random()*10 -5)/1000;
            airParcel StartParcel = new airParcel(1000.0, T0, W0);
            
            double[] CAPE = INS.CAPE(StartParcel);
            double[] SBCAPE = INS.SBCAPE();
            double MLCAPE = INS.MLCAPE();
            double MUCAPE = INS.MUCAPE(SBCAPE[0]);
            
            if (i%1000==0) System.out.format("%d/%d CAPE=%8.2f %n",i,this.Nprofiles,CAPE[0]);
        
        }
        
    }
    
    public static void main(String args[]) throws IOException {
    
        String inputFile = "data/profiles/profile_3042q.txt";
        //int N=160*160;
        int N=100;
        airInstabilityBenchmark bench = new airInstabilityBenchmark(N, inputFile); 
        
        bench.run();
        
    }

}
