/* 
   geoPos.java
   
   Copyright 2019 Olivier Samain

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package atfu;

/**
 * Container class do describe the position of an object in
 * geocentric coordinates.
 * <p> 
 * The geo fields are public so there can be directly read. However there are final so they cannot be modified after initialisation.
 * The intended use is to create new objects every time new coordinates are needed
 * <p>
 * In addition the following utilities are provided:
 * <ul>
 * <li> dist2: square of the euclidian distance between two points
 * <li> gradient: local gradiant for a given variable x on 3 input points
 * <li> extrapolate: extrapolate a variable x from a starting point by providing 
 * the local gradien and a target point
 * <li> geostationaryViewAngles returns the zenith and azimuth view angle for a given point on the earth (assumed spherical) toward a geostationary satellite
 * </ul>
 * <p>
 * Class dependencies:
 * <ul>
 * <li> airGlobals
 * </ul>
 */
public class geoPos implements Cloneable {
    public final double lat;    // Latitude
    public final double lon;    // Longitude
    public final double elev;   // Elevation
    
    // some constants
    private static double earthRadius = 6.371e6;
    private static double geostationaryRadius = 42.164e6;
    private static double deg2rad = Math.PI/180.0;
    
    /** Default constructor (no argument) returning an object with missing fields
     */
    public geoPos() {
        this.lat = airGlobals.MISSING;
        this.lon = airGlobals.MISSING;
        this.elev = airGlobals.MISSING;
    }
    
    /** Constructor taking inputs in double precision
     * @param latitude   latitude (degree)
     * @param longitude  longitude (degree)
     * @param elevation  elevation (m)
     */
    public geoPos(double latitude, double longitude, double elevation) {
        this.lat = latitude;
        this.lon = longitude;
        this.elev = elevation;
    }
    
    /** Clone function implementing the "Cloneable" interface */
    public Object clone() { 
        try {
            geoPos pos = (geoPos)super.clone();
            return pos;
        }
        catch (CloneNotSupportedException e) {
            e.printStackTrace();
	        return null;    
	    }
    }
    
    /** Test object's validity 
     * <p>
     * Check that field are not assigned to airGlobals.MISSING, or that lat / lon have 
     * out of range values
     * @return        Validity flag
     */
    public boolean isValid() {
        if (this.lat==airGlobals.MISSING || this.lon==airGlobals.MISSING || this.elev==airGlobals.MISSING) {
            return false;
        } else if (this.lat>90 || this.lat<-90  || this.lon>360 || this.lon<-180) {
            return false;
        }
        else return true;
    }
    
    public boolean isInvalid() {
        if (this.lat==airGlobals.MISSING || this.lon==airGlobals.MISSING || this.elev==airGlobals.MISSING) {
            return true;
        } else if (this.lat>90 || this.lat<-90  || this.lon>360 || this.lon<-180) {
            return true;
        }
        else return false;
    }
    
    /** Square of the euclidian distance between two points
     * @param pos1    Position of the second point
     * @return        square distance (degree*degree)
     */
    public double dist2(geoPos pos1) {
        return (this.lat- pos1.lat)*(this.lat- pos1.lat) + (this.lon- pos1.lon)*(this.lon- pos1.lon);
    }
    
    /** View zenith and azimuth angles from the current point, toward a geostationary satellite
     * <p>
     * The earth is assumed spherical and elevation is neglected
     * @param satLon    sub-satellite longitude (degree)
     * @return          a 2-element array consisting of {zenith, azimuth} (degrees)
     */
    public double[] geostationaryViewAngles(double satLon) {
    
        // distance between the point and the satellite
        double rr = earthRadius/geostationaryRadius;
        double cosgamma = Math.cos(this.lat*deg2rad)*Math.cos((satLon-this.lon)*deg2rad);
        double satDist = geostationaryRadius*Math.sqrt(1+rr*rr- 2*rr*cosgamma);
        
        // zenith angle
        double singamma = Math.sin(Math.acos(cosgamma));
        double zen = Math.asin(geostationaryRadius/satDist*singamma)/deg2rad;
        
        // azimuth angle
        double alpha = Math.atan(Math.tan((satLon-this.lon)*deg2rad)/Math.sin(this.lat*deg2rad));
        alpha = Math.abs(alpha)/deg2rad;
        //System.out.format("alpha = %8.2f %n", alpha);
        double azi;
        
        // alpha is alway in range [0,90] the azimuth is determined as follows
        if (this.lat<=0) {
            if (this.lon<=satLon) azi=alpha;      // looking north-east
            else azi=360-alpha;                   // looking north-west
        }
        else{
            if (this.lon<=satLon) azi=180-alpha;  // looking south-east
            else azi=180+alpha;                   // looking south-west
        }
        
        double[] out = {zen, azi};
        //System.out.format("zenith = %6.3f azimuth = %6.3f %n", zen,azi);
        return out;
    }
    
    /** Gradient of a variable x against the geoPos lat / lon coordinates
     * <p>
     * Elevation dependency is not taken into account. The method is then solving a rank 2 linear problem.
     * <p>
     * Inputs consist of 3 points and associated value: (this, x) (pos1, x1) and (pos2, x2)
     * @param x       value of x at the first point
     * @param pos1    position of the second point
     * @param x1      value of x at the second point
     * @param pos2    position of the third point
     * @param x2      value of x at the third point
     * @return        object containing the gadients of x against latitude and longitude
     */
    public geoPos gradient(double x, geoPos pos1, double x1, geoPos pos2, double x2) {
      
        // build the linear problem matrix 
        double[][] M = new double[2][2];
        M[0][0]= pos1.lat - this.lat;
        M[0][1]= pos1.lon - this.lon;
        M[1][0]= pos2.lat - this.lat;
        M[1][1]= pos2.lon - this.lon;
        
        
        double det = M[0][0]*M[1][1] - M[0][1]*M[1][0];
        
        if (det==0) {
            // No solution. The 3 points are colinear
            System.out.println("null determinant");
            System.out.format("[%6.2f %6.2f] %n", M[0][0], M[0][1]);
            System.out.format("[%6.2f %6.2f] %n", M[1][0], M[1][1]);
            return new geoPos(0, 0, 0);
        }
        else {
            // perform matric inversion
            double[][] invM = new double[2][2];
            invM[0][0] =  M[1][1]/det;
            invM[0][1] = -M[0][1]/det;
            invM[1][0] = -M[1][0]/det;
            invM[1][1] =  M[0][0]/det;
            
            // dx vector
            double[] dx = {x1-x, x2-x};
            
            // Solution = invM*dx
            double dxdlat = invM[0][0]*dx[0] + invM[0][1]*dx[1];
            double dxdlon = invM[1][0]*dx[0] + invM[1][1]*dx[1];
            
            return new geoPos(dxdlat, dxdlon, 0);
        }
    }
    
    /** Extrapolate a variable x at position targetPos by applying its gradient
     * @param x          value of x at initial position
     * @param gradient   gradient to be applied
     * @param targetPos  target position
     * @return           value of x at the target position  
     */
    // this method extrapolate a variable x at position targetPos by applying its gradient
    public double extrapolate(double x, geoPos gradient, geoPos targetPos) {
        
        // apply gradient from starting point (this,x)
        double dx = (targetPos.lat - this.lat)*gradient.lat + (targetPos.lon - this.lon)*gradient.lon
                  + (targetPos.elev - this.elev)*gradient.elev;
        return x+dx;
    }
    
    /** Print the fields to string */
    public String toString() {
        return String.format("lat= %8.4f lon= %8.4f elev=%7.1f",this.lat, this.lon, this.elev);
    }
    
    /** Print the fields to the standard output */
    public void print() {
        System.out.println(this.toString());
    }
    
    /** 
     * Main function testing the class methods 
     */
    public static void main(String args[]) {
    
        System.out.println("Testing geostationary view angles");
        double lat=20;
        double lon=-60;
        double satLon=0;
        geoPos pos=new geoPos(lat,lon,0.0);
        pos.print();
        double[] viewAngles = pos.geostationaryViewAngles(satLon);
        
        System.out.format("zenith = %6.3f azimuth = %6.3f %n", viewAngles[0],viewAngles[1]);
        
        System.out.println("Testing gradient:");
        geoPos pos0=new geoPos(0,0,0);  // SW corner
        geoPos pos1=new geoPos(1,10,0); // SE corner
        geoPos pos2=new geoPos(10,1,0); // NW corner
        geoPos gradient = pos0.gradient(0,pos1,0, pos2,100);
        gradient.print();
        
        geoPos target=new geoPos(5,5, 0);
        System.out.format("Testing extrapolation at ");
        target.print();
        System.out.format("x= %6.2f %n",pos0.extrapolate(0.0,gradient,target) );
        
    }
    
}
  
