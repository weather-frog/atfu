/*
   profileGeometry.java
   
   Copyright 2019 Olivier Samain
   
   The SPP method in this class is based on the work of T. Vincenty published in:
   "Direct and Inverse Solutions of Geodesics on the Ellipsoid with application of
   nested equations", Survey Review, vol XXII no 176, 1975
   http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
    
*/
package atfu;

/**
 * Provides geographic information and transformation functions relevant for the 
 * manipulation of atmospheric profiles.
 * <p>
 * Class dependencies:
 * <ul>
 * <li> geoPos
 * </ul>
 */
public class profileGeometry implements Cloneable {

    private double surfaceLat;
    private double surfaceLon;
    private double surfaceElev;
    private double zenAngle;
    private double aziAngle;
    
    profileGeometry(geoPos pos, double zen, double azi) {
        this.surfaceLat = pos.lat;
        this.surfaceLon = pos.lon;
        this.surfaceElev = pos.elev;
        this.zenAngle = zen;
        this.aziAngle = azi;
    }
    
    /** Clone function implementing the "Cloneable" interface */
    public Object clone()  { 
        try {
            profileGeometry geo = (profileGeometry)super.clone();
            return geo;
        }
        catch (CloneNotSupportedException e) {
            e.printStackTrace();
	        return null;    
	    }
    }
    
    /** Returns the profile surface point location in geoPos format */
    public geoPos getPos() {
        return new geoPos(this.surfaceLat,this.surfaceLon, this.surfaceElev);
    }
    
    /** Returns the profile surface point latitude */
    public double getLat() {
        return surfaceLat;
    }
    
    /** Returns the profile surface point longitude */
    public double getLon() {
        return surfaceLon;
    }
    
    /** Returns the profile surface point elevation */
    public double getElev() {
        return surfaceElev;
    }
    
    /** Returns the profile view zenith angle */
    public double getZenAngle() {
        return zenAngle;
    }
    
    /** Returns the profile view azimuth angle */
    public double getAziAngle() {
        return aziAngle;
    }
    
    /** Print the fields to the standard output */
    public void print() {
        System.out.format("lat= %8.4f lon= %8.4f elev=%7.1f zen= %6.2f azi= %6.2f %n",this.surfaceLat, this.surfaceLon, this.surfaceElev,this.zenAngle, this.aziAngle);
    }

    /** Sub-Parcel Point
     * <p>  
     * This method takes as input the elevation of a point along a slant profile and calculates 
     * the coordinates of the vertical projection of this point on the earth surface. It is possible 
     * to alter the projection heading by providing a non null value to the 'rotation' input.
     * For instance a rotation of -180 will return the symetric of the spp point with respect to the profile surface point.
     * @param elev      Elevation (m)
     * @param rotation  Rotation angle (degrees)
     * @return          Projection point position
     */
    public geoPos SPP(double elev, double rotation) {

        double theta = 90.0 - this.zenAngle;

        // distance from target to new target defined by reference altitude
        double distance = (elev - this.surfaceElev)/Math.tan(theta*Math.PI/180.0);
        
        return destVincenty(this.surfaceLat, this.surfaceLon, this.aziAngle+rotation, distance) ;
    }
    
    /** Calculate WGS 84 destination given starting lat/long,
    * bearing and distance.
    * <p>
    * from: Vincenty direct formula - T Vincenty, "Direct and Inverse
    * Solutions of Geodesics on the Ellipsoid with application of
    * nested equations", Survey Review, vol XXII no 176, 1975
    * http://www.ngs.noaa.gov/PUBS_LIB/inverse.pdf
    * @param lat1       Starting latitude (degrees)
    * @param lon1       Starting longitude  (degrees)
    * @param bearing    Bearing angle (degrees)
    * @param dist       Distance (m)
    * @return           Projection point position
    */
    private geoPos destVincenty(double lat1, double lon1, double bearing, double dist) {
        // local variable definitions
        // Trigonometric constants and conversion factors
        // ----------------------------------------------
        double toDEG = 180.0/Math.PI;
        double toRAD = 1.0/toDEG;
        // WGS-84 ellipsiod
        double a=6378137.0;
        double b=6356752.3142;
        double f=1/298.257223563;
        double cosSigma=0;
        double sinSigma=0;
        double cos2SigmaM=0;

        // code body
        double alpha1 = bearing*toRAD;
        double sinAlpha1 = Math.sin(alpha1);
        double cosAlpha1 = Math.cos(alpha1);
        double tanU1 = (1-f) * Math.tan(lat1*toRAD);
        double cosU1 = 1 / Math.sqrt((1 + tanU1*tanU1));
        double sinU1 = tanU1*cosU1;
        double sigma1 = Math.atan2(tanU1, cosAlpha1);
        double sinAlpha = cosU1 * sinAlpha1;
        double cosSqAlpha = 1 - sinAlpha*sinAlpha;
        double uSq = cosSqAlpha * (a*a - b*b) / (b*b);
        double A = 1 + uSq/16384*(4096+uSq*(-768+uSq*(320-175*uSq)));
        double B = uSq/1024 * (256+uSq*(-128+uSq*(74-47*uSq)));
        double sigma = dist / (b*A);
        double sigmaP = 2*Math.PI;
        
        while (Math.abs(sigma-sigmaP) > 1e-12) {
            cos2SigmaM = Math.cos(2*sigma1 + sigma);
            sinSigma = Math.sin(sigma);
            cosSigma = Math.cos(sigma);
            double deltaSigma = B*sinSigma*(
                                cos2SigmaM+B/4*(cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)
                                                -B/6*cos2SigmaM*(-3+4*sinSigma*sinSigma)
                                                *(-3+4*cos2SigmaM*cos2SigmaM))
                                           );
            sigmaP = sigma;
            sigma = dist / (b*A) + deltaSigma;
        }
        double tmp = sinU1*sinSigma + cosU1*sinSigma*cosAlpha1;
        double lat2 = Math.atan2(sinU1*cosSigma + cosU1*sinSigma*cosAlpha1,
                         (1-f)*Math.sqrt(sinAlpha*sinAlpha + tmp*tmp));
        double lambda = Math.atan2(sinSigma*sinAlpha1,
                            cosU1*cosSigma - sinU1*sinSigma*cosAlpha1);
        double C = f/16*cosSqAlpha*(4+f*(4-3*cosSqAlpha));
        double L = lambda-(1-C)*f*sinAlpha*
                 ( sigma+C*sinSigma*(cos2SigmaM+C*cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)) );

        // check for invalid outputs
        Double lat = new Double(lat2);
        Double lon = new Double(L);
        if (Double.isNaN(lat) || Double.isNaN(lon)) {
            System.out.format("Warning: SPP invalid outputs (NaN) for %9.5f %9.5f %9.5f %9.5f %n",lat1, lon1, bearing, dist);
            return new geoPos();
        } else {
            // return new position in degrees
            return new geoPos(lat2*toDEG,lon1+(L*toDEG),0.0);
        }
    }
    
    /** 
     * Main function testing the class methods 
     */
    public static void main(String args[]){
    
        geoPos pos = new geoPos(45.0, 0.0, 0.0);
        pos.print();
        
        profileGeometry geometry = new profileGeometry(pos, 45.0, 180.0);
        
        geometry.SPP(11000.0, 0).print();
    
    }



}
