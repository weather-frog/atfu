


import net.sourceforge.chart2d.*;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.Color;
import java.util.Random;
import java.io.IOException;


/**
 * A Chart2D demo demonstrating the LBChart2D object.
 * Container Class: JFrame<br>
 * Program Types:  Applet or Application<br>
 */
public class displayProfiles extends JApplet {


  private JFrame frame = null;
  private static boolean isApplet = true;


  /**
   * For running as an application.
   * Calls init() and start().
   * @param args An unused parameter.
   */
  public static void main (String[] args) throws IOException {

    isApplet = false;
    displayProfiles panel = new displayProfiles();
    panel.init();
    panel.frame.show();
    //exit on frame close event
  }
  
  /**
   * Configure the chart and frame, and open the frame.
   */
  public void init() {

    //Start configuring a JFrame GUI with a JTabbedPane for multiple chart panes
    JTabbedPane panes = new JTabbedPane();

    String input="../data/profiles/profile_q423.txt";
    panes.addTab ("Line", getProfileChart(input));
  

    //JTabbedPane specific GUI code
    //Chart2D by default magnifies itself on user resize, the magnification
    //raio is based on each chart's preferred size.  So that all the charts
    //have the same magnification ratio, we must make all the charts have the
    //same preferred size.  You can calculate each chart's preferred size
    //dynamically (slower), OR you can pick a size that is at least a big as
    //its dynamic preferred size and use this statically -- permanently.
    //I recommend using dynamic calc while writing your code, then for
    //production make sure to use the static code for the performance increase.
    //Add a System.out.println (size) with the dynamic code, and use that size
    //for your static code size.
    //Also, setting the panes preferred size with a static size pushes
    //calculation of charts in non-visible panes off, until they are visible.
    //This means that start up time with a static panes size is the same as if
    //you had only one Chart2D object.
    boolean dynamicSizeCalc = false;
    if (dynamicSizeCalc) {
      int maxWidth = 0;
      int maxHeight = 0;
      for (int i = 0; i < panes.getTabCount(); ++i) {
        Chart2D chart2D = (Chart2D)panes.getComponentAt (i);
        chart2D.pack();
        Dimension size = chart2D.getSize();
        maxWidth = maxWidth > size.width ? maxWidth : size.width;
        maxHeight = maxHeight > size.height ? maxHeight : size.height;
      }
      Dimension maxSize = new Dimension (maxWidth, maxHeight);
      System.out.println (maxSize);
      for (int i = 0; i < panes.getTabCount(); ++i) {
        Chart2D chart2D = (Chart2D)panes.getComponentAt (i);
        chart2D.setSize (maxSize);
        chart2D.setPreferredSize (maxSize);
      }
      System.out.println (panes.getPreferredSize());
    }
    else {
      Dimension maxSize = new Dimension (561, 214);
      for (int i = 0; i < panes.getTabCount(); ++i) {
        Chart2D chart2D = (Chart2D)panes.getComponentAt (i);
        chart2D.setSize (maxSize);
        chart2D.setPreferredSize (maxSize);
      }
      panes.setPreferredSize (new Dimension (566 + 5, 280 + 5)); //+ 5 slop
    }

    frame = new JFrame();
    frame.getContentPane().add (panes);
    frame.setTitle ("LBChart2DFrameDemo");
    frame.addWindowListener (
      new WindowAdapter() {
        public void windowClosing (WindowEvent e) {
          destroy();
    } } );
    frame.pack();
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    frame.setLocation (
      (screenSize.width - frame.getSize().width) / 2,
      (screenSize.height - frame.getSize().height) / 2);
  }

  /**
   * Ends the application or applet.
   */
  public void destroy() {

    if (frame != null) frame.dispose();
    if (!isApplet) System.exit (0);
  }
  
  /**
   * Builds the demo chart.
   * @return The demo chart.
   */
  private Chart2D getProfileChart(String inputFile) {

    
    
    
    //<-- Begin Chart2D configuration -->

    //Configure object properties
    Object2DProperties object2DProps = new Object2DProperties();
    String TitleString = inputFile.substring(inputFile.lastIndexOf("/") + 1);
    System.out.println(TitleString);
    object2DProps.setObjectTitleText (TitleString);

    //Configure chart properties
    Chart2DProperties chart2DProps = new Chart2DProperties();

    //Configure legend properties
    LegendProperties legendProps = new LegendProperties();
    String[] legendLabels = {"Temperature", "Water-vapour", " "};
    legendProps.setLegendLabelsTexts (legendLabels);

    //Configure graph chart properties
    GraphChart2DProperties graphChart2DProps = new GraphChart2DProperties();
    String[] labelsAxisLabels =
      {"1100", "1000", "900", "800", "700", "600",
       "500", "400", "300", "200", "100", "0"};
    graphChart2DProps.setLabelsAxisLabelsTexts (labelsAxisLabels);
    graphChart2DProps.setLabelsAxisTitleText ("Pressure");
    graphChart2DProps.setNumbersAxisTitleText ("Programmers");
    graphChart2DProps.setChartDatasetCustomizeGreatestValue (true);
    graphChart2DProps.setChartDatasetCustomGreatestValue (1100);
    graphChart2DProps.setChartDatasetCustomizeLeastValue (true);
    graphChart2DProps.setChartDatasetCustomLeastValue (0);
    graphChart2DProps.setLabelsAxisTicksAlignment (graphChart2DProps.CENTERED);
    

    //Configure graph properties
    GraphProperties graphProps = new GraphProperties();
    graphProps.setGraphBarsExistence (false);
    graphProps.setGraphLinesExistence (true);
    graphProps.setGraphOutlineComponentsExistence (true);
    graphProps.setGraphAllowComponentAlignment (true);

    //Configure dataset
    Dataset dataset = new Dataset (3, 12, 1);
    dataset.set (0,  0, 0, 100f);  //1990
    dataset.set (0,  1, 0, 200f);  //1991
    dataset.set (0,  2, 0, 300f);  //1992
    dataset.set (0,  3, 0, 400f);  //1993
    dataset.set (0,  4, 0, 100f);  //1994
    dataset.set (0,  5, 0, 200f);  //1995
    dataset.set (0,  6, 0, 100f);  //1996
    dataset.set (0,  7, 0,   0f);  //1997
    dataset.set (0,  8, 0, 100f);  //1998
    dataset.set (0,  9, 0, 100f);  //1999
    dataset.set (0, 10, 0, 200f);  //2000
    dataset.set (0, 11, 0, 300f);  //2001
    dataset.set (1,  0, 0,   0f);  //1990
    dataset.set (1,  1, 0,   0f);  //1991
    dataset.set (1,  2, 0,   0f);  //1992
    dataset.set (1,  3, 0, 100f);  //1993
    dataset.set (1,  4, 0, 200f);  //1994
    dataset.set (1,  5, 0, 400f);  //1995
    dataset.set (1,  6, 0, 500f);  //1996
    dataset.set (1,  7, 0, 700f);  //1997
    dataset.set (1,  8, 0, 900f);  //1998
    dataset.set (1,  9, 0, 100f);  //1999
    dataset.set (1, 10, 0, 200f);  //2000
    dataset.set (1, 11, 0, 300f);  //2001
    dataset.set (2,  0, 0,   0f);  //1990
    dataset.set (2,  1, 0,   0f);  //1991
    dataset.set (2,  2, 0,   0f);  //1992
    dataset.set (2,  3, 0,   0f);  //1993
    dataset.set (2,  4, 0, 100f);  //1994
    dataset.set (2,  5, 0, 200f);  //1995
    dataset.set (2,  6, 0, 300f);  //1996
    dataset.set (2,  7, 0, 400f);  //1997
    dataset.set (2,  8, 0, 500f);  //1998
    dataset.set (2,  9, 0, 100f);  //1999
    dataset.set (2, 10, 0, 300f);  //2000
    dataset.set (2, 11, 0, 900f);  //2001
    
    //Configure graph component colors
    MultiColorsProperties multiColorsProps = new MultiColorsProperties();
    
    //Configure chart
    LBChart2D chart2D = new LBChart2D();
    chart2D.setObject2DProperties (object2DProps);
    chart2D.setChart2DProperties (chart2DProps);
    chart2D.setLegendProperties (legendProps);
    chart2D.setGraphChart2DProperties (graphChart2DProps);
    //chart2D.addGraphProperties (graphProps);
    //chart2D.addDataset (dataset);
    //chart2D.addMultiColorsProperties (multiColorsProps);
    
    // read data
    try{
        airProfile profile = new airProfile(inputFile);
        System.out.println("levels: " + profile.Nlev);
    
        Dataset temperature = new Dataset(1,profile.Nlev,1);
        for (int i=0;i<profile.Nlev;i++){
            temperature.set(0,i,0,(float)profile.T[i]);
        }
        Dataset pressure = new Dataset(1,profile.Nlev,1);
        for (int i=0;i<profile.Nlev;i++){
            pressure.set(0,i,0,(float)profile.P[i]);
        }
        
        
        //Configure graph properties
        GraphProperties profileProps = new GraphProperties();
        profileProps.setGraphBarsExistence (false);
        profileProps.setGraphLinesExistence (true);


        //Configure graph component colors
        MultiColorsProperties PressureColors = new MultiColorsProperties();
        PressureColors.setColorsCustomize (true);
        PressureColors.setColorsCustom (new Color[] {new Color (193, 183, 0)});
        
        MultiColorsProperties temperatureColors = new MultiColorsProperties();
        temperatureColors.setColorsCustomize (true);
        temperatureColors.setColorsCustom (new Color[] {new Color (193, 183, 0)});
        
        // add graph
        chart2D.addDataset (pressure);
        chart2D.addMultiColorsProperties (PressureColors);
        chart2D.addGraphProperties (profileProps);
        
        
    }
    catch (Exception e) {
            System.out.println("Message catch: " + e.getMessage());
    }
    

    //Optional validation:  Prints debug messages if invalid only.
    if (!chart2D.validate (false)) chart2D.validate (true);

    //<-- End Chart2D configuration -->

    return chart2D;
    
  }
  
}
