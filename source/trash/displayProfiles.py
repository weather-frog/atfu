#!/usr/bin/env python2
import sys

#sys.path.append("/usr/lib/python2.7/site-packages/")
#sys.path.append("/usr/lib64/python2.7/site-packages/")
#sys.path.append("/usr/lib64/python2.7/site-packages/gtk-2.0")

#print(sys.path)
import numpy as np
import matplotlib.pyplot as plt

# open file
f = open('../data/profiles/profile_q423.txt', 'r')

# read header 1
f.readline()

# read surface pressure
line=f.readline()
columns = (line.strip()).split()
Ps = columns[0]

# read header 2
f.readline()

# read nb of levels
line=f.readline()
columns = (line.strip()).split()
Nlev = int(columns[0])

# initialize arrays
levels=np.zeros(Nlev)
pressure=np.zeros(Nlev)
temperature=np.zeros(Nlev)
watervapour=np.zeros(Nlev)

# read header 3
f.readline()

# read level values
for i in range(0,Nlev):
    line=f.readline()
    columns = (line.strip()).split()
    levels[i]=i
    pressure[i]=columns[0]
    temperature[i]=columns[1]
    watervapour[i]=columns[2]

plt.plot(pressure,temperature)
plt.show()

f.close()
