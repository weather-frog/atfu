// Hello.java (Java SE 5)
import javax.swing.*;

public class displayTest extends JFrame {
    public displayTest() {
        super("hello");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.add(new JLabel("Hello, world!"));
        this.pack();
        this.setVisible(true);
    }

    public static void main(final String[] args) {
        new displayTest();
        
    }
}
