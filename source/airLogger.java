 /*  
   airLogger.java
   
   Copyright 2019 Olivier Samain

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package atfu;

import java.util.logging.*;
import java.io.File;
import java.io.IOException;

public class airLogger {
    
    public static Logger getLogger(String className, String logFile, String logLevel) throws IOException {
        // define custom logging format
        System.setProperty("java.util.logging.SimpleFormatter.format","%2$s %4$s:  %5$s %n");
            
        // create object logger (will use the above custom format)
        Logger logger = Logger.getLogger(className);
            
        // create a handler for log file
        FileHandler handler = new FileHandler(logFile, false);
        SimpleFormatter formatter = new SimpleFormatter();
        handler.setFormatter(formatter);
        
        // attache handler to logger
        logger.addHandler(handler);
        
        Level log = Level.OFF;
        switch(logLevel) {
                case "INFO": log=Level.INFO; break;
                case "WARNING": log=Level.WARNING; break;
                case "FINE": log=Level.FINE; break; 
                case "FINEST": log=Level.FINEST; break;
                case "ALL": log=Level.ALL; break;
                default: log=Level.WARNING; break;
        }
            
        // set the log level (WARNING, INFO, FINE, ALL)
        logger.setLevel(log);
            
        // prevent duplicated messages to the console
        logger.setUseParentHandlers(false);
        
        return logger;
    
    }
    
    /** Check actual log level against target
     * <p>
     * Use the Level intValue, which is lowest for "ALL" 
     * @param logger   Current logger
     * @param level    Target level 
     * @return True if logger has a lower (i.e deeper) log level
     */
    public static boolean isLower(Logger logger, Level level) {
        
        if (logger.getLevel().intValue() < level.intValue()) return true;
        else return false;
    }
    
    public static void closeAll() {
        LogManager.getLogManager().reset();
    }
    
    public static void main(String args[]) throws IOException {
    
    String className = airLogger.class.getName();
    
    Logger log = getLogger(className,"test.log","ALL");
    
    
    log.info("test");
    
    log.setLevel(Level.WARNING);
    
    log.warning("test fine");
    
    isLower(log,Level.FINEST);
    isLower(log,Level.FINE);
    isLower(log,Level.INFO);
    
    LogManager.getLogManager().reset();
    
    }

}
