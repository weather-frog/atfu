/*
   airParcel.java
 
   Copyright 2019 Olivier Samain

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
    
*/
package atfu;

import java.util.Locale;

/**
 * Defines a parcel of air and the methods for various conversions
 * and transormations.
 * <p>
 * The key fields parcel properties for the various calculations are: Pressure, Temperature, Water-vapour mass mixing ratio and a validity flag set by the constructor. Geographic information can be added to an existing parcel with addPosition() and is used by the spatial interpolation method interp4P(). The Ozone and CO2 mass-mixing ratios can also be specified, for information storage, and are supported in the spatial interpolation.
 * <p> 
 * Notable functions:
 * <ul>
 * <li> relative or specific humidity
 * <li> dew point temperature
 * <li> virtual temperature
 * <li> water saturation presure
 * <li> lifting condensation level
 * </ul> 
 * <p>  
 * Class dependencies:
 * <ul>
 * <li> airGlobals
 * <li> geoPos
 * <li> fast2DInt
 * </ul>
*/
public class airParcel {
    
    private double P;   // Pressure (hPa)
    private double T;   // Temperature (K)
    private double W;   // Water-Vapour mass-mixing ratio (kg/kg)
    private double O;   // Ozone mass-mixing ratio (kg/kg)
    private double C;   // CO2 mass-mixing ratio (kg/kg)
    private geoPos pos=null;    // Geographic information
    private boolean validity;   // Parcel validity flag
 
    /** Default constructor
     * <p>
     * Takes no inputs and return an object with missing (airGlobals.MISSING) values. Validity flag is set to 'false'.
     */
    public airParcel(){
        this.T=airGlobals.MISSING;
        this.P=airGlobals.MISSING;
        this.W=airGlobals.MISSING;
        this.O=airGlobals.MISSING;
        this.C=airGlobals.MISSING;
        this.validity=false;
    }
    
    /** Partial constructor
     * <p>
     * Takes Pressure, Temperature and Water-vapour inputs. The other fields are set to zero.
     * @param P     Pressure (hPa)
     * @param T     Temperature (K)
     * @param W     Water-vapour mass mixing ratio (kg/kg)
     */
    public airParcel(double P, double T, double W)
    {
        this.P=P;
        this.T=T;
        this.W=W;
        this.O=0;
        this.C=0;
        this.validity=true;
    }
    
    /** Full constructor
     * <p>
     * All parcel fields are set
     * @param P     Pressure (hPa)
     * @param T     Temperature (K)
     * @param W     Water-vapour mass mixing ratio (kg/kg)
     * @param O     Ozone mass mixing ratio (kg/kg)
     * @param C     CO2 mass mixing ratio (kg/kg)
     */
    public airParcel(double P, double T, double W, double O, double C)
    {
        this.P=P;
        this.T=T;
        this.W=W;
        this.O=O;
        this.C=C;
        
        // check pressure validity
        if (P == airGlobals.MISSING || P<0)  this.validity=false;
        else this.validity=true;
    }

    /** Set / overwrite the pressure field
     * @param P     Pressure (hPa)
     */
    public void setP(double P) {
        this.P = P;
    }
    
    /** Set / overwrite the temperature field
     * @param T     Temperature (K)
     */
    public void setT(double T) {
        this.T = T;
    }
    
    /** Set / overwrite the Water-vapour field
     * @param W     Water-vapour mass mixing ratio (kg/kg)
     */
    public void setW(double W) {
        this.W = W;
    }
    
    /** Set / overwrite the Ozone field.
     * @param O     Ozone mass mixing ratio (kg/kg)
     */
    public void setO(double O) {
        this.O = O;
    }
    
    /** Set / overwrite the CO2 field.
     * @param C     CO2 mass mixing ratio (kg/kg)
     */
    public void setC(double C) {
        this.C = C;
    }
    
    /** Add geographic information to an existing parcel
     * @param position     Parcel lat/lon/elevation coordinates
     */
    public void addPosition(geoPos position) {
        this.pos = (geoPos) position.clone();
    }
    
    /** Get the pressure field.
     * @return   Pressure (hPa)
     */
    public double getP() {
        return this.P;
    }
    
    /** Get the temperature field.
     * @return   Temperature (K)
     */
    public double getT() {
        return this.T;
    }
    
    /** Get the water-vapour field.
     * @return   Water-vapour mass mixing ratio (kg/kg)
     */
    public double getW() {
        return this.W;
    }
    
    /** Get the ozoner field.
     * @return   Ozone mass mixing ratio (kg/kg)
     */
    public double getO() {
        return this.O;
    }
    
    /** Get the CO2 field.
     * @return   CO2 mass mixing ratio (kg/kg)
     */
    public double getC() {
        return this.C;
    }
    
    /** Get the parcel geographic information
     * @return   Position
     */
    public geoPos getPos() {
        return this.pos;
    }
    
    /** Get the parcel validity flag
     * @return   Validity
     */
    public boolean isValid() {
        return this.validity;
    }
    
    /** Print parcel fields to string */
    public String toString() {
        if (this.pos!=null) {
            return String.format("P= %6.2f T= %6.2f W= %8.5f %s",P,T,W,this.pos.toString());
        } else {
            return String.format("P= %6.2f T= %6.2f W= %8.5f %s",P,T,W,"No geoPos data");
        }
    }
    
    /** Print parcel fields to the standard output */
    public void print() {
        System.out.println(this.toString());
    }
    
    /** Parcel specific humidity
     * @return   Specific humidity (kg/kg)
     */
    public double q() {
        return this.W/(1+this.W);
    }
    
    /** Parcel moist air gas constant. This value depends on the parcel humidity
     * @return   Moist air gas constant (J/kg K)
     */
    public double Rm() {
        return airGlobals.RL*(1+0.608*this.q());
    }
    
    /** Parcel moist air specific heat. This value depends on the parcel humidity.
     * @return   Moist air specific heat (J/kg/K)
     */
    public double Cpm() {
        return airGlobals.Cp*(1+0.887*this.q());
    }
    
    /** Dry adiabatic temperature change, following a change of pressure
     * @param P1    New pressure (hPa)
     * @return      New temperature (K)
     */
    public double DryAdiabT(double P1){
        return this.T*Math.pow(P1/this.P,Rm()/Cpm());
    }
    
    /** Dry adiabatic pressure change, following a change of temperature
     * @param T1    New temperature (K)
     * @return      New pressure (hPa)
     */
    public double DryAdiabP(double T1){
        return this.P/Math.pow(this.T/T1, Rm()/Cpm());
    }
    
    /** Parcel virtual temperature
     * @return      Virtual temperature (K)
     */
    public double VT(){
        return this.T*(1+0.608*this.q());
    }
    
    /** Parcel water-vapour partial pressure
     * @return      H20 pressure (hPa)
     */
    public double PH2O() {
       return this.P/(1+airGlobals.MH2O/airGlobals.ML*(1+this.W)/this.W);
    }
    
    /** Parcel water-vapour saturation pressure above water
     * @return      Water-vapour saturation pressure (hPa)
     */
    public double SatPressureWater() {
        double Ts=373.16;
        double ews= 1013.246;
        double C1 = -7.90298;
        double C2 = 5.02808;
        double C3 = -1.3816e-7;
        double C4 = 11.344;
        double C5 = 8.1328e-3;
        double C6 = -3.49149;
        
        double log_es=Math.log10(ews);
        log_es+= C1*(Ts/this.T -1);
        log_es+= C2*Math.log10(Ts/this.T);
        log_es+= C3*(Math.pow(10,C4*(1-this.T/Ts)) - 1);
        log_es+= C5*(Math.pow(10,C6*(Ts/this.T-1)) - 1);
        
        return Math.pow(10,log_es);
    }
    
    /** Parcel water-vapour saturation pressure above ice
     * @return      Water-vapour saturation pressure (hPa)
     */
    public double SatPressureIce() {
        double ei0= 6.1071;
        double C1 = -9.09718;
        double C2 = -3.56654;
        double C3 = 0.876793;
        
        double log_es=Math.log10(ei0);
        log_es+= C1*(airGlobals.T0/this.T -1);
        log_es+= C2*Math.log10(airGlobals.T0/this.T);
        log_es+= C3*(1-this.T/airGlobals.T0);
        
        System.out.println("Ice es= " + Math.pow(10,log_es));
        
        return Math.pow(10,log_es);
    }
    
    /** Parcel relative humidity
     * @return      Relative humidity (%)
     */
    public double rh() {
        return 100*this.PH2O()/this.SatPressureWater();
    }
    
    /**
     * Parcel dew point temperature
     * @return      Dew-point temperature (K)
     */
    public double DPT() {
        double TH1=243.15;
        double[] A ={6.116441, 6.114742} ;
        double[] m ={7.591386, 9.778707} ;
        double[] Tn={240.7263, 273.1466} ;
        
        int i=(this.T<TH1)?1:0;
        
        return Tn[i]/(m[i]/Math.log10(this.PH2O()/A[i])-1)+airGlobals.T0;
    }
    
    /** Convert dew point temperature to water-vapour mass-mixing ratio
     * @param DPT   Dew-point temperature (K)
     * @return      water-vapour mass-mixing ratio (Kg/kg)
     */
    public double DP2W(double DPT) {
        double k1=airGlobals.T0;
        double k2=243.15;
        double k3=17.67;
        double k4=1000/621.97/6.112;
        
        return Math.exp(k3*(DPT-k1)/(DPT+k2-k1))/(k4*this.P);
    }
    
    /** Parcel lifting Condensation Level
     * @return      a 2-element array consisting of {TLCL, PLCL}, respectively the condensation level temperature (K) and pressure (hPa)
     */
    public double[] LCL() {
        double rh=this.rh();
        double TLCL=1/(1/(this.T-55)-Math.log⁡(rh/100)/2840);
        double PLCL=this.DryAdiabP(TLCL);
        
        double[] out={TLCL,PLCL};
        return out;
    }
    
    /** Parcel equivalent potential Temperature
     * @param TLCL  Lifted concensation level temperature (K)
     * @return      Equivalent Potential Temperature (K)
     */
    public double EPT(double TLCL) {
        
        // dry adiabatic potential temperature
        double theta = this.T*Math.pow(1000/this.P,Rm()/Cpm());
        
        return theta*Math.exp⁡( (3.376/TLCL -0.00254)*1000*this.W
                              *(1+0.81*Math.pow(10,-3)*this.W) );
    }
    
    
    /** Interpolation between 4 surrounding parcels
     * <p>
     * At least one pacel shall have a validity flag set to 'true' and valid lat/lon coordinates
     * <p>
     * This method relies on the fast2DInt interpolation class
     * @param corners   A 4 elements array containing airParcel objects
     * @param target    Interpolation target location
     * @return          Interpolated parcel
     */
    public static airParcel interp4P(airParcel[] corners, geoPos target) {
        airParcel IntpParcel=null;
        
        if (corners.length !=4) {
            System.out.println("wrong input array size");
            IntpParcel = new airParcel(); // missing parcel
        } else {
            // build inputs for 2D interpolation
            int Nvar=5;
            double[] Xin=new double[4];
            double[] Yin=new double[4];
            double[][] VarIn=new double[4][Nvar];
            byte[] mask = new byte[4];
            double[] XYout = {target.lat, target.lon};
            int nvalid = 0;
            
            for (int n=0;n<4;n++) {
                if (corners[n].validity==false || corners[n].pos == null) {
                    // Invalid parcel or no geographic information available.
                    mask[n]=0;
                }
                else if (corners[n].pos.lat==airGlobals.MISSING || corners[n].pos.lat==airGlobals.MISSING) {
                    // invalid coordinates
                    mask[n]=0;
                } else { 
                    mask[n]=1;
                    Xin[n] = corners[n].pos.lat;
                    Yin[n] = corners[n].pos.lon;
                    VarIn[n][0] = corners[n].P;
                    VarIn[n][1] = corners[n].T;
                    VarIn[n][2] = corners[n].W;
                    VarIn[n][3] = corners[n].O;
                    VarIn[n][4] = corners[n].C;
                    nvalid++;
                }
            }
            if (nvalid>0) {
                // call generic interpolation function
                double[] VarOut = fast2DInt.GEN(Nvar, Xin, Yin, VarIn, mask, XYout);
                IntpParcel = new airParcel(VarOut[0], VarOut[1], VarOut[2], VarOut[3], VarOut[4]);
                IntpParcel.addPosition(target);
            }
            else {
                //System.out.println("Parcel interpolation WARNING: no valid corner");
                IntpParcel = new airParcel();
            }
        }
        return IntpParcel;
    }

    /** 
     * Main function testing the class methods 
     */    
    public static void main(String args[]){
        // force locale to avoid issues with reading / writing numbers 
        Locale l = new Locale("en", "GB");
        Locale.setDefault(l);
        
        // Instanciate
        airParcel parcel=new airParcel(1013.0, 313.15, 0.0240);
        //airParcel parcel=new airParcel(1013.0, 313.1, 0.01);
        System.out.println("Parcel properties:");
        parcel.print();
        
        System.out.println("");
        System.out.println("Derived variables:");
        System.out.format("   PH2O= %8.2f %n", parcel.PH2O());
        System.out.format("   rh= %8.2f %n", parcel.rh());
        System.out.format("   DPT= %8.2f %n", parcel.DPT());
        System.out.println("   Validation: Vaisala example for 40°C, 1013hPa and rh=50");
        System.out.format(" ->DPT= %8.2f %n" ,240.7263/(7.591386/Math.log10(36.88/6.116441)-1)+273.15);
        System.out.format("   W(DPT)= %8.5f %n", parcel.DP2W(parcel.DPT()));
        
        System.out.format("   EPT= %8.2f %n", parcel.EPT(313.15));
        
        
        System.out.format("Lifting condensation level:");
        double[] LCL=parcel.LCL();
        System.out.format("   TLCL= %8.2f %n", LCL[0]);
        System.out.format("   PLCL= %8.2f %n", LCL[1]);
        
        airParcel[] corners = new airParcel[4];
        geoPos spp = new geoPos(5.0203, 0.0352,0.0);
        
        corners[0] = new airParcel(225.73,227.56,0.00010,0,0);
        corners[0].addPosition(new geoPos(5.0094,0.0351,0));
        
        corners[1] = new airParcel(225.73,227.56,0.00010,0,0);
        corners[1].addPosition(new geoPos(5.0188,0.0703,0));
        
        corners[2] = new airParcel(225.73,227.56,0.00010,0,0);
        corners[2].addPosition(new geoPos(5.0445,0.0257,0));
        
        corners[3] = new airParcel(225.73,227.56,0.00010,0,0);
        corners[3].addPosition(new geoPos(5.0539,0.0608,0));
        
        airParcel intpParcel = interp4P(corners,spp);
        System.out.println("");
        intpParcel.print();
    }

}
