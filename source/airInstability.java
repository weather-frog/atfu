 /*  
   airInstability.java
   
   Copyright 2019 Olivier Samain

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package atfu;

import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Arrays;
import java.util.logging.*;
import java.util.Properties;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Scanner;

/**
 * Calculation of various instability indices for a given atmospheric profile
 * <p> 
 * Extends airProfile. Note that only the P, T and W fields are used here.
 * <p> 
 * For debugging purposes, a logging functionality is implemented (constructor taking
 * a log file and a log level as inputs. The possible levels are: WARNING, INFO, FINE
 * and FINEST. The latter two have significant performance impact.
 * <p>
 * Class dependencies:
 * <ul>
 * <li> airGlobals
 * <li> airParcel
 * <li> airProfile
 * </ul>
 */
public class airInstability extends airProfile{

    // default start parcel for CAPE calculations
    private airParcel StartParcel = new airParcel(1000, 300.0,0.02);
    
    // custom logger
    private static Logger logger = null;
    
    // flag used to totally disable call to logger if the log level is not FINE or FINEST. Huge performance impact.
    private static boolean trace = false;
    
    /** Constructor that creates the logger and calls the parent class constructor airParcel(int Nlev)
     * <p>
     *  The profile fields are created but initialized to missing values
     * @param Nlev      Number of vertical levels
     */
    public airInstability(int Nlev) throws IOException {
        super(Nlev);
    }    
    
    /** Constructor that initialize profile data from an input file
     * <p>
     * See example files in atfu/data/profiles/
     * @param inputFile Input file (ASCII)
     */
    public airInstability(String inputFile, double Ts) throws IOException {
        super(inputFile,0);
        
        // set surface skin temperature
        this.Ts = Ts;
    }
    
    /** Set logging parameters   
     * @param trace    Enable logging if set to true (default=false)
     * @param logFile   Log file path
     * @param logLevel WARNING, INFO, FINE or FINEST (default=WARNING)
     */
    public static void setLogging(boolean trace, String logFile, String logLevel) throws IOException { 
        airInstability.trace = trace;
        
        if (airInstability.logger==null) { 
            System.out.println("Creating logger to "+ logFile);
            airInstability.logger = airLogger.getLogger(airInstability.class.getName(),logFile, logLevel);
            airInstability.logger.info("Loggin set with level" + logLevel);
        }
    }
    
    /** From an existing airInstability object, copy fields from another profile
     * @param profile   Profile to copy
     */
     public void airProfileCopy(airProfile profile){

        this.Nlev = profile.Nlev;
        this.Ps = profile.Ps;
        this.Ts = profile.Ts;
        this.Sl = profile.Sl;
        this.gi = profile.gi;
        this.h = profile.h;
        this.P = new double[Nlev];
        this.T = new double[Nlev];
        this.W = new double[Nlev];
        
        for (int lev = 0; lev < Nlev; lev++) {
            P[lev]=profile.P[lev];
            T[lev]=profile.T[lev];
            W[lev]=profile.W[lev];
        }
        
        if (profile.getGeometry() != null) this.geometry = (profileGeometry)profile.getGeometry().clone();
    }
    
    /** Lifted Index
     * @return LI (K)
     */
    public double LI() {
        final double PLI = 500;     // Lifted pressure target (hPa)
        final double SFC_DP = 100;  // Surface parcel pressure range
        final double[] c1 = {-8.8416605e-03, +3.6182989e-03};
        final double[] c2 = {1.4714143e-04, -1.3603273e-05};
        final double[] c3 = {-9.6719890e-07, +4.9618922e-07};
        
        double LI=airGlobals.MISSING;
        
        // calculate the air temperature at 500hpa
        double[] Pout = {PLI};
        double[] TLI = VINT("T",Pout);
        
        // The index is undefined is first level above surface is < 500 hPa
        if (this.P[this.Sl]<PLI) {
            System.out.format("LI warning. First level found lower than %8.4f. LI undefined. %n",PLI);
            return airGlobals.MISSING;
        }
        
        // Calculate the surface air parcel pressure, temperature and water-vapour mixing ratio as the respective averages over the first 100 hPa above the surface
        int lev=Math.min(this.Sl,this.Nlev-1);
        int count=0;
        double Psfc=0;
        double Tsfc=0;
        double Wsfc=0;
        while (this.P[lev]>=(this.Ps-SFC_DP) && lev>=0 ) {
            count++;
            Psfc+=this.P[lev];
            Tsfc+=this.T[lev];
            Wsfc+=this.W[lev];
            lev--;
        }
        if (count > 0) {
            Psfc /= count;
            Tsfc /= count;
            Wsfc /= count;
        }
        else {
            if (trace) this.logger.log(Level.FINE,String.format("LI warning. First level found (%8.3f) is higher than PS - %8.4f hPa = %8.3f. No computation possible. %n",this.P[Sl],SFC_DP,this.Ps-SFC_DP));
            System.out.format("%d %8.3f %8.3f %n", Sl, this.P[Sl],this.Ps-SFC_DP );
            return airGlobals.MISSING;
        }
        
        // calculate the lifted condensation level
        airParcel SfcParcel= new airParcel(Psfc, Tsfc, Wsfc);
        double[] LCL=SfcParcel.LCL(); // returns {TLCL, PLCP}
        //debug 
        //System.out.println("LI / TLCL, PLCL= " + LCL[0] +" , " + LCL[1]);
        double Tlifted=0;
        
        if (LCL[1]<PLI) {
            // LCP pressure < 500 hpa. Use dry adiabatic lapse rate
            Tlifted = SfcParcel.DryAdiabT(PLI);
            // debug 
            //System.out.println("LI / Tlifted (dry) = " + Tlifted);
        }
        else {
            // surface potential temperature
            double theta = SfcParcel.EPT(LCL[0]);
            
            // temperature that a totally dry air parcel of temperature theta at 1000 hPa would have if adiabatically displaced to 500 hPa
            airParcel DryParcel = new airParcel(1000,theta,0);
            double T1 = DryParcel.DryAdiabT(PLI);
            
            // empirical formula for the lifted temperature
            double DT=T1-293.16;
            double P4=0;
            double T2=0;
            if (DT<0) {
                P4=Math.pow(1+c1[0]*DT+c2[0]*DT*DT+c3[0]*DT*DT*DT,4);
                T2=15.13/P4;
            }
            else{
                P4=Math.pow(1+c1[1]*DT+c2[1]*DT*DT+c3[1]*DT*DT*DT,4);
                T2=29.93/P4+0.96*DT-14.8;
            }
            Tlifted = T1 - T2;
            //System.out.println("LI / Tlifted (moist) = " + Tlifted);
        }
        
        // Final index calculation
        LI=TLI[0]-Tlifted;
        
        return LI;
    }
    
    /** KI Index
     * @return KI (K)
     */
    public double KI() {
        final double Plow=850;   // lower pressure bound
        final double Pmid=700;   // mid pressure bound
        final double Phigh=500;  // upper pressure bound
        
        // Interpolate temperature and water vapour to the pressure bounds
        double[] Pair={Phigh, Pmid, Plow};
        double[] Tair = VINT("T",Pair);
        double[] Wair = VINT("W",Pair);
        
        // Dew point temperatures
        airParcel lowParcel = new airParcel(Plow,Tair[2],Wair[2]);
        airParcel midParcel = new airParcel(Pmid,Tair[1],Wair[1]);
        double DPTlow = lowParcel.DPT();
        double DPTmid = midParcel.DPT();
        
        double KI=(Tair[2]-Tair[0])+DPTlow-(Tair[1]-DPTmid)-273.15;
    
        return KI;
    }
    
    /** Layer Precipitable Water
     * <p> Returns a 3-elements array containing the LPW content for layers 0 to 850hPa,
     * 850 to 500hPa and 500hPa to top of atmosphere
     * @param elev  Surface elevation
     * @param lat   Air Parcel latitude
     * @return LPW (kg/m²)
     */
    public double[] LPW( double elev,  // elevation (m)
                         double lat){  // latitude (°)
        
        final double LPW_low = 850;       // lower pressure level (hPa)
        final double LPW_mid = 500;       // mid pressure level (hPa)
        final double LPW_high = 0;        // high pressure level (hPa)
        
        // water-vapour vertical integration
        String[] VarIds = {"W"};
        double[] VarBot = {airGlobals.MISSING};
        double[] WC1 = TCOL(VarIds,this.Ps,LPW_low,elev,lat,VarBot);
        double[] WC2 = TCOL(VarIds,LPW_low,LPW_mid,elev,lat,VarBot);
        double[] WC3 = TCOL(VarIds,LPW_mid,LPW_high,elev,lat,VarBot);
        
        double[] LPW={WC1[0],WC2[0], WC3[0]};
        
        return LPW;
    }
    
    /** Maximum buoyancy
     * <p>
     * Returns an array where the first element is the max buyancy and 
     * the other Nlev elements are the equivalent potential temperature for each level 
     * lifted to their condensation level.
     * @return MB (K)
     */
    public double[] MB(){
        final double MB_MAX_top = 850;        // Max search top pressure (hPa)
        final double MB_MIN_bottom = 700;     // Min search bottom pressure (hPa)
        final double MB_MIN_top = 300;        // Min search top pressure (hPa)
        
        double[] MB = new double[this.Nlev+1];  // first element is the MB index
                                                // next Nlev elements are the equivalent potential temperatures
        
        // equivalent potential temperatures at each level between the surface and MB_MIN_top 
        for (int k=0; k<this.Nlev;k++) {
            
            if (k<=this.Sl && P[k]>=MB_MIN_top && P[k]!=airGlobals.MISSING) {
                // lifted condensation level
                airParcel parcel = new airParcel(this.P[k],this.T[k],this.W[k]);
                double[] LCL=parcel.LCL();  // returns {TLCL, PLCL}
            
                // equivalent potential temperature
                MB[k+1] = parcel.EPT(LCL[0]);
                //System.out.format("MB [%3d] = %6.2f, %n", k, MB[k+1]);
            }
            else MB[k+1] = airGlobals.MISSING;
        }
        
        // Loop on levels to find the maximum equivalent potential temperature in the lower troposphere, from surface to MB_top1, and the minimum equivalent temperature in the free troposphere from MB_MIN_bottom to MB_MIN_top.
        double EPTmin=999;
        double EPTmax=-999;
        for (int k=this.Sl; this.P[k]>MB_MAX_top;k--) {
            if (MB[k+1]>EPTmax) EPTmax=MB[k+1];
        }
        for (int k=this.Sl; this.P[k]>MB_MIN_top;k--) {
            // the loop starts for the surface but the search actually starts from MB_MIN_bottom
            if (this.P[k]<MB_MIN_bottom &&  MB[k+1]<EPTmin) EPTmin=MB[k+1];
        }
        MB[0] = EPTmax - EPTmin;
        
        return MB;
    }
    
    /** Difference of Equivalent potential temperature
     * <p>
     * Returms the difference between the max and min equivalent potential temperatures
     * over a certain pressure range
     * @param THe Equivalent potential temperature at all pressure levels (array of size Nlev)
     * @return DTHe (K)
     */
    public double DTHe(double[] THe) {
    
        final double DTHE_MAX_DP = 100;    // max search delta pressure over surface
        final double DTHE_MIN_TOP = 500;   // min search top pressure
        
        // check input array size
        if (THe.length != this.Nlev) {
            throw new IllegalArgumentException ("DTH2 input has wrong size");
        }
        
        // max initialisation
        double THeMax = -999;
        int k = this.Sl;
        
        // max search loop
        while (this.P[k]> (this.Ps-DTHE_MAX_DP)) {
            if (THe[k]!=airGlobals.MISSING && THe[k]>THeMax) THeMax=THe[k];
            k--;
        }
        
        // min initialisation
        double THeMin = 999;
        k=this.Sl;
        while (this.P[k]> DTHE_MIN_TOP) {
            if (THe[k]!=airGlobals.MISSING && THe[k]<THeMin) THeMin=THe[k];
            k--;
        }
        
        return THeMax - THeMin;
    }
    
    /** Convective Available Potential Energy
     * <p>
     * Returns an array of two elements: [CAPE, CIN] where CAPE represents the 
     * convection energy and CIN the convection inhibition
     * @param StartParcel Initial air parcel properties
     * @return CAPE
     */
    public double [] CAPE (airParcel StartParcel){ 
        
        final double CAPE_MIN_P = 100;    // Search loop minimum pressure
        final double CAPE_MAX_ITER = 15;   // Search loop max number of iterations
        final double CAPE_MIN_ITER = 3;   // Search loop max number of iterations
        final double CAPE_MIN_DTG = 0.1;    // Search loop pressure increment threshold
 
        // initialisations
        double CAPE=0;
        double CIN=0;
        
        double CpAccum=0;
        double P0=StartParcel.getP();
        double T0=StartParcel.getT();
        double W0=StartParcel.getW();
        double WG0 = W0;
        double TG0 = T0;;
        boolean LFC = false; // flag to say whether the free convection layer has been reached
        boolean EL = false;  // flag to say whether the equilibrium layer has been reached 
        
        // Characterise the LCL corresponding to the air parcel at original level
        double [] LCL = StartParcel.LCL();
        double PLCL = LCL[1];
        
        // parcel virtual temperature at initial level. Make a copy of it.
        double VTpcl0 = StartParcel.VT();
        double VTenv0 = VTpcl0;
        
        // parcel saturation pressure at initial level
        double es0 = StartParcel.SatPressureWater();
        
        // water-vapour partial pressue at initial level
        double ev0 = StartParcel.PH2O();
        
        // pseudo adiabatic entropy at initial level
        double S0 = 1005.7*Math.log(T0) - 287.04*Math.log(P0-ev0)
                    + (2.501e6 - 2320*(T0-airGlobals.T0))*WG0/T0
                    - 461.5*WG0*Math.log(ev0/es0);
        
        if (trace) this.logger.log(Level.FINEST,String.format("S0(1) = %8.5f",1005.7*Math.log(T0) - 287.04*Math.log(P0-ev0)));
        if (trace)this.logger.log(Level.FINEST,String.format("S0(2) = %8.5f",(2.501e6 - 2320*(T0-airGlobals.T0))*WG0/T0));
        if (trace) this.logger.log(Level.FINEST,String.format("S0(3) = %8.5f",- 461.5*WG0*Math.log(ev0/es0)));

        // end initialisations
        
        // loop over levels above the initial one
        //***************************************
        // find firts level above P0
        int k=this.Sl-1;
        while (this.P[k]>P0) {
            k--;
        }
        
        // temp variables
        double VTenv=0;
        double Tlifted=0;
        double VTpcl=0;
        double esk=0;
        double esG=0;
        double Wsat=0;
        double Lk=0;
        double SLk=0;
        double TG=0;
        double DTG=0;
        double DTGmax=0;
        double WG=0;
        double evG=0;
        double Sk=0;
        double Cp=0;
        double APE=0;
        int i=0;
        int icount=0;
        int imean=0;
        
        outerloop:
        while (this.P[k] > CAPE_MIN_P && k>0 && !EL) {
            if (trace) this.logger.log(Level.FINE,"");
            if (trace) this.logger.log(Level.FINE,String.format("Level %3d  P= %8.3f",k,this.P[k]));
            
            // new air parcel correspondingto the target level
            airParcel envParcel = new airParcel(this.P[k], this.T[k], this.W[k]);
            
            // check parcel validity
            if (!envParcel.isValid()) {
                // we have reached a missing / invalid level. Stop the loop here.
                this.logger.log(Level.WARNING,String.format("Reached invalid / missing parcel at level %d",k));
                break outerloop;
            }
            
            // new environment virtual temperature
            VTenv = envParcel.VT();
            Tlifted = StartParcel.DryAdiabT(this.P[k]);
            airParcel liftedParcel = new airParcel(P[k], Tlifted, WG0);
            double rh = liftedParcel.rh();
            if (trace) this.logger.log(Level.FINE,String.format("rh= %6.2f",rh));
            
            //if (this.P[k] >= PLCL) {
            if (rh <= 100) {
                // the ascent to level k is a dry adiabatic one
                //this.logger.log(Level.FINE,"dry adiabatic lifting");
                VTpcl = liftedParcel.VT();
                
                if (trace) this.logger.log(Level.FINE,String.format("T0=%8.5f Tlifted = %8.5f VTpcl= %8.5f",T0,Tlifted,VTpcl));
                                
            }
            else {
                /* The lifted parcel follows a moist adiabatic ascent. The lifted parcel temperature and mixing ratio along the pseudo adiabatic ascent up to level k is solved iteratively.*/
                if (trace) this.logger.log(Level.FINE,"moist adiabatic lifting");
                
                // saturation pressure at level k
                esk = envParcel.SatPressureWater();
                
                // saturation mixing ratio at level k
                Wsat = airGlobals.MH2O * esk / airGlobals.ML / (this.P[k]-esk);
                //this.logger.log(Level.FINEST,String.format("WG0=%8.5f Wsat = %8.5f",WG0,Wsat));
                
                // latent heat of vaporisation in the environment at level k
                Lk = 2.501e6 - 2320*(this.T[k]-airGlobals.T0);
                
                //  Intermediate value for the Pseudo adiabatic entropy
                SLk = (1005.7 + 4190*Wsat + Lk*Lk*Wsat/(461.5*this.T[k]*this.T[k]))
                      /this.T[k];
                      
                // initialise lifted parcel to the environment values
                TG = this.T[k];
                WG = Wsat;
                liftedParcel.setT(TG);
                liftedParcel.setW(WG);
                
                if (trace) this.logger.log(Level.FINE,String.format("Loop starting point: TG = %8.5f WG= %8.5f",TG,WG));
                
                // iteration
                DTG=CAPE_MIN_DTG*2;
                for (i=0;i<CAPE_MAX_ITER && (Math.abs(DTG)>CAPE_MIN_DTG || i<CAPE_MIN_ITER);i++) {
                    
                    //System.out.format("TG = %8.5f TG0=%8.5f %n",TG,TG0);
                    Cp = CpAccum + 4190*(WG + WG0)/2*Math.log(TG/TG0);
                    // new partial water-vapour pressure
                    evG = liftedParcel.PH2O();
                    
                    // pseudo-adiabatic entropy increment
                    Sk = 1005.7*Math.log(TG) - 287.04*Math.log(this.P[k]-evG)
                       + (2.501e6-2320*(TG-airGlobals.T0))*WG/TG + Cp;
                    
                    if (trace) this.logger.log(Level.FINEST,String.format("Sk(1) = %8.5f",1005.7*Math.log(TG) - 287.04*Math.log(this.P[k]-evG)));
                    if (trace) this.logger.log(Level.FINEST,String.format("Sk(2) = %8.5f",(2.501e6-2320*(TG-airGlobals.T0))*WG/TG));
                    if (trace) this.logger.log(Level.FINEST,String.format("Sk(3) = %8.5f",Cp));
                    if (trace) this.logger.log(Level.FINEST,String.format("Sk = %8.5f S0= %8.5f SLk= %8.5f",Sk,S0,SLk));
                    
                    // new temperature
                    DTG = (S0-Sk)/SLk; 
                    TG+=DTG;
                    if (trace) this.logger.log(Level.FINEST,String.format("TG+ = %8.5f",(S0-Sk)/SLk));
                    
                                    
                    // update parcel temperature
                    liftedParcel.setT(TG);
                    
                    // new saturation pressure
                    esG = liftedParcel.SatPressureWater();
                    
                    // new saturation mass-mixing ratio
                    WG = airGlobals.MH2O * esG / airGlobals.ML / (this.P[k]-esG);
                    //System.out.format("TG = %8.5f WG= %8.5f esG = %8.5f  %n",TG, WG, esG);
                    
                    if (TG<=0) {
                        System.out.format("CAPE non-convergence P= %8.4f TG = %8.3f WG= %8.5f esG = %8.5f %n",this.P[k],TG, WG, esG);
                        this.logger.log(Level.WARNING,String.format("CAPE non-convergence P= %8.4f TG = %8.3f WG= %8.5f esG = %8.5f %n",this.P[k],TG, WG, esG));
                        CAPE=airGlobals.MISSING;
                        //StartParcel.print();
                        //this.print();
                        break outerloop;
                    }
                    
                    // update parcel water-vapour
                    liftedParcel.setW(WG);
                    
                }
                if (trace) this.logger.log(Level.FINE,String.format("Loop end point:      TG = %8.5f WG= %8.5f",TG,WG));
                
                // Convergence statistics
                if (trace) this.logger.log(Level.FINE,String.format("Convergence: %3d iterations, Last DTG= %8.5f", i+1, DTG));
                DTGmax = Math.max(DTGmax,Math.abs(DTG));
                icount+=1;
                imean+=i+1;
                
                if (WG > WG0) this.logger.log(Level.INFO,String.format("W increasing: %8.5f vs %8.5f", WG, WG0));
                
                // set final values for the lifted air parcel
                Tlifted = TG;
                VTpcl = liftedParcel.VT();
                
                if (VTpcl > VTpcl0) this.logger.log(Level.INFO,String.format("VT increasing: %8.5f vs %8.5f level=%d", VTpcl, VTpcl0,k));
                
                 // update initial values for the next pressure level
                TG0 = TG;
                WG0 = WG;
                CpAccum = Cp;
            }
            
            // available potential energy for the last layer (between k and k-1)
            APE = airGlobals.RL*( (VTpcl0+VTpcl)/2 - (VTenv0+VTenv)/2 )
                  * (this.P[k]-this.P[k-1])/(this.P[k]+this.P[k-1]);
            
            // replace environment and parcel temperatures
            VTenv0 = VTenv;
            VTpcl0 = VTpcl;
            
            /* check whether the lifted parcel has reached the level of free 
            convection (LFC) or the equilibrium level (EL)*/
            if (trace) this.logger.log(Level.FINE,String.format("VTpcl= %8.5f VTenv= %8.5f W= %8.5f LFC= %b",VTpcl, VTenv, WG0,LFC));
            
            if (VTpcl<VTenv && !LFC) {
                // the level is below LFC. Increment CIN.
                CIN+=APE;
                if (trace) this.logger.log(Level.FINE,String.format("CIN+ = %8.5f",APE));
                            }
            else if (VTpcl<VTenv && LFC) {
                // EL has been reached
                if (!EL) this.logger.log(Level.INFO,String.format("EL has been reached at P[%3d]= %8.5f",k,P[k]));
                //System.out.format("EL has been reached at P[%3d]= %8.5f %n",k,P[k]);
                EL = true;
            }
            else {
               // LFC has been reached. Increment CAPE
               if (!LFC) this.logger.log(Level.INFO,String.format("LFC has been reached at P[%3d]= %8.5f",k,P[k]));
               LFC = true;
               CAPE+=APE;
               if (trace) this.logger.log(Level.FINE,String.format("APE+ = %8.5f",APE));
            }
            
            // level index decrease
            k--;
        }
        
        //***************************************
        // end level loop
        
        // convergence statistics
        if (icount>0) {
            this.logger.log(Level.INFO,String.format("Worst convergence: DTGmax= %8.5f",DTGmax));
            this.logger.log(Level.INFO,String.format("Mean nb of iterations = %8.3f",(float)imean/icount));
        }
        // final result
        this.logger.log(Level.INFO,String.format("CAPE = %8.3f  CIN= %8.3f",CAPE,CIN));
        
        double[] out={CAPE,CIN};
        return out;
    }
    
    /** Surface Based CAPE
     * <p>
     * Returns an array of two elements: [CAPE, CIN] where CAPE represents the 
     * convection energy and CIN the convection inhibition.
     * <p>
     * In this case the initial parcel is the surface parcel. 
     * @return SBCAPE
     */
    public double[] SBCAPE() {
        // in this case the start parcel is just the first level above the surface
        int lev = this.Sl;
        airParcel SBStartParcel = new airParcel(this.P[lev],this.T[lev],this.W[lev]);
        double[] CAPE = this.CAPE(SBStartParcel);
        
        return CAPE;
    }
    
    /** Mixed Layer CAPE
     * <p>
     * Returns an array of two elements: [CAPE, CIN] where CAPE represents the 
     * convection energy and CIN the convection inhibition.
     * <p>
     * In this case the initial parcel is the mean of the first 100hPa above the surface 
     * @return MLCAPE
     */
    public double MLCAPE() {
        final double SFC_DP = 100;  // Surface parcel pressure range
        
        // Calculate the surface air parcel pressure, temperature and water-vapour mixing ratio as the respective averages over the first 100 hPa above the surface
        int lev=Math.min(this.Sl,this.Nlev);
        int count=0;
        double Psfc=0;
        double Tsfc=0;
        double Wsfc=0;
        while (this.P[lev]>=(this.Ps-SFC_DP) && lev>=0 ) {
            count++;
            Psfc+=this.P[lev];
            Tsfc+=this.T[lev];
            Wsfc+=this.W[lev];
            lev--;
        }
        if (count > 0) {
            Psfc /= count;
            Tsfc /= count;
            Wsfc /= count;
        }
        else {
            if (trace) this.logger.log(Level.FINE,String.format("MLCAPE warning. First level found (%8.3f) is higher than PS - %8.4f hPa = %8.3f. No computation possible. %n",this.P[Sl],SFC_DP,this.Ps-SFC_DP));
            return airGlobals.MISSING;
        }
        
        // call the generic CAPE algorithm
        airParcel MLStartParcel = new airParcel(Psfc,Tsfc,Wsfc);
        //airParcel MLStartParcel = new airParcel(Psfc,285.0,0.05);
        double[] CAPE = this.CAPE(MLStartParcel);
        
        return CAPE[0];
    }
    
    /** Maximum CAPE
     * <p>
     * Returns the maximum CAPE by looping trough all starting levels up to 300hPa above the surface
     * @param SBCAPE Surface Based CAPE (see SBCAPE method)
     * @return MUCAPE (K)
     */
    public double MUCAPE(double SBCAPE) {
        
        // In this case the start parcel is defined by a loop between the surface and MUCAPE_DP above the surface. The maximum value is retained.
        final double MUCAPE_DP = 300;
        int k = this.Sl-1;
        double maxCAPE=SBCAPE;
        while (this.P[k]>=(this.Ps-MUCAPE_DP)) {
            this.logger.log(Level.INFO,String.format("MUCAPE level %d", k));
            airParcel MUStartParcel = new airParcel(this.P[k],this.T[k],this.W[k]);
            double[] CAPE = this.CAPE(MUStartParcel);
            if (Math.abs(CAPE[0])>Math.abs(maxCAPE)) maxCAPE=CAPE[0];
            k--;
        }
        
        return maxCAPE;
    }

    /** 
     * Main function testing the class methods 
     */
    public static void main(String args[]) throws IOException {
        // Force locale to avoid issues with reading / writing numbers 
        Locale l = new Locale("en", "GB");
        Locale.setDefault(l);
        
        Properties prop = new Properties();
        InputStream propFile = null;
        String profileDir="data/profiles/";
        String profileID="0423q";
        String logDir="./";
        String logLevel = "FINE";
        
        // Surface defaults
        double Ts = 305;
        double elev=0;        
        double StartParcelP0=1000;
        double StartParcelT0=300;
        double StartParcelW0=0.01;
        
        try {
            // load properties file
            propFile = new FileInputStream("config/airInstability.properties");
            prop.load(propFile);

            // get the property values
            String value;
            value = prop.getProperty("logDir");
            if (value !=null) {
                File testDir = new File(value);
                if (testDir.isDirectory()) { 
                    logDir=value;
                }
                else {
                    // use the local directory
                    System.out.println("Invalid log directory: Use default");
                }
            }
            
            value = prop.getProperty("logLevel");
            if (value !=null) logLevel=value;
            
            value = prop.getProperty("profileDir");
            if (value !=null) profileDir=value;
            
            value = prop.getProperty("profileID");
            if (value !=null) profileID=value;
            
            value = prop.getProperty("Ts");
            if (value !=null) Ts=Double.parseDouble(value);
            
            value = prop.getProperty("elev");
            if (value !=null) elev=Double.parseDouble(value);
            
            value = prop.getProperty("StartParcelP0");
            if (value !=null) StartParcelP0=Double.parseDouble(value);
            
            value = prop.getProperty("StartParcelT0");
            if (value !=null) StartParcelT0=Double.parseDouble(value);
            
            value = prop.getProperty("StartParcelW0");
            if (value !=null) StartParcelW0=Double.parseDouble(value);

        } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println("airInstability.properties not found or corrupted: Use default values");
        } finally {
            if (propFile != null) propFile.close();
        }
        
        // set logging
        String inputFile = String.format("%s/profile_%s.txt", profileDir, profileID);
        String logFile = String.format("%s/airInstability.log", logDir);
        airInstability.setLogging(true, logFile, logLevel);
        airProfile.setLogging(true, "logs/airProfile.log","INFO");
                
        // Create a profile object from file
        airInstability INS = new airInstability(inputFile,Ts);
        
        // print the date of the log file creation
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat ("yyyy/MM/dd 'at' hh:mm:ss");

        airInstability.logger.info("File created on " + dateFormat.format(date));
        
        System.out.format("%nTesting LI index: %n");
        System.out.format("  LI = %6.2f %n",INS.LI());
        
        System.out.format("%nTesting KI index: %n");
        System.out.format("  KI = %6.2f %n",INS.KI());
        
        System.out.format("%nTesting LPW index: %n");
        double lat=5;
        double[] LPW=INS.LPW(elev,lat);
        System.out.format("  Ts = %8.5f elev = %8.5f lat = %8.5f %n",Ts,elev,lat);
        System.out.format("  LPW = %8.5f %8.5f %8.5f %n",LPW[0],LPW[1],LPW[2]);
        
        System.out.format("%nTesting MB index: %n");
        double[] MB = INS.MB();
        System.out.format("  MB = %6.2f %n", MB[0]);
        
        System.out.format("%nTesting DTHe index: %n");
        
        System.out.format("  DTHe = %6.2f %n",INS.DTHe(Arrays.copyOfRange(MB,1,MB.length)) );
        
        System.out.format("%nTesting CAPE functions: %n");
        
        System.out.format("%nUser defined start parcel : %n");
        airParcel StartParcel = new airParcel(StartParcelP0, StartParcelT0, StartParcelW0);
        StartParcel.print();
        System.out.format("   rh= %6.2f %n",StartParcel.rh());
        double[] LCL=INS.StartParcel.LCL();
        System.out.format("   TLCL= %6.2f, PLCL= %6.2f %n", LCL[0], LCL[1]);
        double[] CAPE = INS.CAPE(StartParcel);
        System.out.format("   CAPE = %8.5f CIN= %8.5f %n",CAPE[0],CAPE[1]);
        
        System.out.format("%nsurface start parcel : %n");
        double[] SBCAPE = INS.SBCAPE();
        System.out.format("  SBCAPE = %8.5f CIN= %8.5f %n",SBCAPE[0],SBCAPE[1]);
        
        System.out.format("%n100hPa surface average start parcel : %n");
        double MLCAPE = INS.MLCAPE();
        System.out.format("  MLCAPE = %8.5f %n",MLCAPE);
        
        System.out.format("%n300hPa loop over surface : %n");
        double MUCAPE = INS.MUCAPE(SBCAPE[0]);
        System.out.format("  MUCAPE = %8.5f %n",MUCAPE);
        
        LogManager.getLogManager().reset();
        
 
    }
 
}
