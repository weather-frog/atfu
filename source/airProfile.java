/*
   airProfile.java
   
   Copyright 2019 Olivier Samain

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
package atfu;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Locale;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.logging.*;

/**
 * Definition of an atmospheric profile and associated methods
 * <p>
 * It is possible to add geometric information (surface point, slant angle)
 * to an existing profile with addGeometry()
 * <p>
 * Example profiles are available in <atfu_root>/data/profiles/
 * <p>
 * Class dependencies:
 * <ul>
 * <li> airGlobals
 * <li> airParcel
 * </ul>
*/
public class airProfile implements Cloneable {
    
    /** Surface pressure (hPa)*/
    protected double Ps;
    
    /** Surface skin temperature (K)*/
    protected double Ts;
    
    /** Number of vertical levels */
    protected int Nlev; 
    
    /** index of first level above surface */
    protected int Sl;
    
    /** pressure profile (hPa)*/
    protected double[] P;
    
    /** temperature profile (K)*/
    protected double[] T;
    
    /** water-vapour mass-mixing ratio profile (kg/kg) */
    protected double[] W;
    
    /** ozone mass-mixing ratio profile (kg/kg)*/
    protected double[] O;
    
    /** CO2 mass-mixing ratio profile (kg/kg) */
    protected double[] C;
    
    protected profileGeometry geometry = null;
    
    private int Mlev;
    
    protected int gi;      // gravity integration index (temporary variable used for P2HL method)
    protected double h;    // latest calculated height (temporary variable used for P2HL method)
    
    
    // custom logger
    private static Logger logger = null;
    
    // flag used to totally disable call to logger if the log level is not FINE or FINEST. Huge performance inpact.
    private static boolean trace = false;
    
    /** Constructor returning an empty profile
     * <p> 
     * Fields arrays are allocated but all set to missing values (airGlobals.MISSING)
     * @param Nlev  Number of vertical levels
     */
    public airProfile(int Nlev) {
        this.Nlev = Nlev;
        this.Ps =airGlobals.MISSING;
        this.Ts =airGlobals.MISSING;
        this.P = new double[Nlev];
        this.T = new double[Nlev];
        this.W = new double[Nlev];
        this.O = new double[Nlev];
        this.C = new double[Nlev];
        this.gi=-1;
        this.h=0;
        this.Mlev=0;
        
        for (int lev = 0; lev < Nlev; lev++) {
            P[lev]=airGlobals.MISSING;
            T[lev]=airGlobals.MISSING;
            W[lev]=airGlobals.MISSING;
        }
        
    }
    
    /** Constructor that initialize profile data from an input file
     * <p>
     * See example files in atfu/data/profiles/
     * @param inputFile Input file (ASCII)
     * @param minLev    Minimun number of levels to be created
     */
    public airProfile(String inputFile, int minLev) throws IOException {
        
        Scanner fileScanner= null;
        int linesRead=0;
        float x;
        String comment;
        
        // check is the logger is already defined
        //if (logger==null) logger = airLogger.getLogger(airInstability.class.getName(),this.logFile, Level.WARNING);
        
        if (trace) System.out.println("Reading from " + inputFile);
        if (trace) logger.info("Reading from " + inputFile);
        
        try {
            fileScanner = new Scanner(new File(inputFile)).useLocale(Locale.UK);
            
            // Read comment line
            if(fileScanner.hasNextLine()) {
                comment=fileScanner.nextLine();
            }
            else throw new IOException("Wrong input file format: Can't read next line");
            
            // Read the surface pressure
            if(fileScanner.hasNextLine()) {
                this.Ps = fileScanner.nextFloat();
                //System.out.println("surf pressure="+this.Ps);
                fileScanner.nextLine();
            }
            else throw new IOException("Wrong input file format: Can't read next line");
           
            // Read comment line
            if(fileScanner.hasNextLine()) {
                comment=fileScanner.nextLine();
            }
            else throw new IOException("Wrong input file format: Can't read next line");
            // Read the number of levels
            if(fileScanner.hasNextLine()) {
                this.Nlev = Math.max(fileScanner.nextInt(), minLev);
                //System.out.println("Levels ="+this.Nlev);
                fileScanner.nextLine();
                
            // Allocate profiles
                this.P = new double[Nlev];
                this.T = new double[Nlev];
                this.W = new double[Nlev];
                this.C = new double[Nlev];
                this.O = new double[Nlev];
            }
            else throw new IOException("Wrong input file format: Can't read next line");
            
            // Read comment line
            if(fileScanner.hasNextLine()) {
                comment=fileScanner.nextLine();
            }
            else throw new IOException("Wrong input file format: Can't read next line");
            
            while (fileScanner.hasNextLine() && linesRead<Nlev)
            {
                // read profile fields for one line
                P[linesRead] = fileScanner.nextFloat();
                T[linesRead] = fileScanner.nextFloat();
                W[linesRead] = fileScanner.nextFloat();
                O[linesRead] = fileScanner.nextFloat();
                C[linesRead] = fileScanner.nextFloat();
                
                fileScanner.nextLine();
                linesRead++;
            }
            
            // fill missing levels
            for (int k=linesRead; k<Nlev;k++) {
                P[k] = airGlobals.MISSING;
                T[k] = airGlobals.MISSING;
                W[k] = airGlobals.MISSING;
                O[k] = airGlobals.MISSING;
                C[k] = airGlobals.MISSING;
            }
            
            // find the first level above the surface. This will skip all the missing levels, if any.
            Sl=Nlev-1;
            while (P[Sl]>Ps) Sl--;
            
            // set skin temperature
            this.Ts = this.T[Sl];
            
            // set gravity integration index to an invalid value.
            gi=-1;
            
            // set missing levels
            Mlev=0;
            
        }
        catch (Exception e) {
            System.out.println("airProfile constructor: " + e.getMessage());
            throw new IOException("Error reading input file");
        }
        finally {
            if(fileScanner!= null) fileScanner.close();
        }
    }
    
    /** Set logging parameters   
     * @param trace    Enable logging if set to true (default=false)
     * @param logFile   Log file path
     * @param logLevel WARNING, INFO, FINE or FINEST (default=WARNING)
     */
    public static void setLogging(boolean trace, String logFile, String logLevel) throws IOException { 
        airProfile.trace = trace;
        
        if (trace && logger==null) { 
            airProfile.logger = airLogger.getLogger(airProfile.class.getName(),logFile, logLevel);
        }
    }
    
    /** Clone function implementing the "Cloneable" interface   */
    public Object clone()  { 
        try {
            airProfile profile = (airProfile)super.clone();
            
            // deep cloning
            if (geometry!=null) profile.geometry = (profileGeometry) geometry.clone();
                
            return profile;
        }
        catch (CloneNotSupportedException e) {
            e.printStackTrace();
	        return null;    
	    }
    }
    
    /** Returns a field value for a given level
     * <p> 
     * @param VarId Field ID ("P", "T", "W", "O" or "C")
     * @param level Vertical level
     */
    public double getLevel(String VarId, int level) {
        double output=airGlobals.MISSING;
        if (level>=0 && level <Nlev) {
          switch(VarId) {
            case "P": output=this.P[level]; break;
            case "T": output=this.T[level]; break;
            case "W": output=this.W[level]; break;
            case "O": output=this.O[level]; break;
            case "C": output=this.C[level]; break;
            default: throw new IllegalArgumentException("Invalid input field:"+VarId); 
          }
        }
        else throw new IllegalArgumentException(String.format("Invalid level index: %d",level)); 
        return output;
    }
    
    public void setInvalid() {
        this.Mlev=Nlev;
    }
    
    /** Set surface parameters
     * @param Ps Surface pressure
     * @param Ts Surface skin temperature
     * @param Sl Index of first level above the surface if known, max level otherwise
     */
    public void setSurface(double Ps, double Ts, int Sl) throws IllegalArgumentException {
            try {
                if (Ps<0) throw new IllegalArgumentException("Negative Ps:"+Ps);
                if (Ts<0) throw new IllegalArgumentException("Negative Ts:"+Ps);
                if (Sl<0) throw new IllegalArgumentException("Sl is out of bounds:"+Sl);
            }
            catch (IllegalArgumentException ex) {
                ex.printStackTrace();
                throw new IllegalArgumentException("Invalid argument");
            }
            
            this.Ps = Ps;
            this.Ts = Ts;
            
            // Re-calculate Sl if needed
            int i=Math.min(Sl,this.Nlev-1);
            while (this.P[i]>Ps){
                //System.out.format("%5d %12.7f %12.7f %12.8f %n",i,Ps,P[i],this.P[i]-Ps);
                i--;
                }
            this.Sl = i;
            
    }
    
    /** Set all field values for a given level
     * @param level Vertical level
     * @param P     Level pressure
     * @param T     Water-vapour mass mixing ratio (kg/kg)
     * @param O     Ozone mass mixing ratio (kg/kg)
     * @param C     CO2 mass mixing ratio (kg/kg)
     */
    public void setLevel(int level, double P, double T, double W, double O, double C) throws IllegalArgumentException {
        try {
            if(level<0 || level>=this.Nlev) throw new IllegalArgumentException("Invalid level argument:"+level);
            if (P<0) throw new IllegalArgumentException("Negative P:"+P);
            if (T<0) throw new IllegalArgumentException("Negative T:"+T);
            if (W<0) throw new IllegalArgumentException("Negative W:"+W);
            if (O<0) throw new IllegalArgumentException("Negative O:"+O);
            if (C<0) throw new IllegalArgumentException("Negative C:"+C);
            
            this.P[level] = P;
            this.T[level] = T;
            this.W[level] = W;
            this.O[level] = O;
            this.C[level] = C;
        }
        catch (IllegalArgumentException ex) {
                ex.printStackTrace();
                throw new IllegalArgumentException("Invalid argument");
        }
    }
    
    /** Set all field values for a given level
     * @param level  Vertical level
     * @param parcel airParcel object
     */
    public void setLevel(int level, airParcel parcel) throws IllegalArgumentException {
        try {
            if(level<0 || level>=this.Nlev) throw new IllegalArgumentException("level is out of bounds:"+level);
            // we assume here that parcel has valid data
            this.P[level] = parcel.getP();
            this.T[level] = parcel.getT();
            this.W[level] = parcel.getW();
            this.O[level] = parcel.getO();
            this.C[level] = parcel.getC();
        }
        catch (IllegalArgumentException ex) {
                ex.printStackTrace();
                throw new IllegalArgumentException("Invalid argument");
        }
    }
    
    /** Add a geometry to an existing profile
     * @param pos      Position
     * @param zenAngle Profile zenith angle
     * @param azimAngle Profile azimuth angle
     */
    public void addGeometry(geoPos pos, double zenAngle, double azimAngle) {
            this.geometry = new profileGeometry(pos, zenAngle, azimAngle);
    }
    
    /** Add a geometry to an existing profile
     * @param geo      profileGeometry object
     */
    public void addGeometry(profileGeometry geo) {
    
        this.geometry = (profileGeometry)geo.clone();
    }
    
    /** Returns the profile geometry
     * @return profileGeometry object
     */
    public profileGeometry getGeometry() {
        return geometry;
    }
    
    /** Returns the number of vertical levels
     * @return Nlev
     */
    public int getNlev() {
        return this.Nlev;
    }
    
    /** Returns the number of missing levels
     * @return Nlev
     */
    public int getMissingLevels() {
        return this.Mlev;
    }
    
     /** Increment the number of missing levels */
    public void addMissingLevel() {
        this.Mlev++;
    }
    
    /** Returns the surface pressure
     * @return Ps
     */
    public double getPs() {
        return this.Ps;
    }
    
    /** Returns the surface skin temperature
     * @return Ts
     */
    public double getTs() {
        return this.Ts;
    }
    
    /** Returns the index of first level above the surface
     * @return Sl
     */
    public int getSl() {
        return this.Sl;
    }
    
    /** Print the full profile to standard output */
    public void print() {
        double elev=0.0;
        if (this.geometry != null) elev = this.geometry.getElev();
        System.out.format("Surface Ps= %8.2f Ts= %8.3f Sl= %d elev= %8.1f %n ",this.Ps, this.Ts, this.Sl, elev);
        System.out.println("level  P     T     W     C      O");
        for (int k=0; k<this.Nlev;k++) printLevel(k);
    }
    
    /** Print vertical level to standard output
     * @param level Vertical level
     */
    public void printLevel(int level) {
        System.out.format("%4d %8.2f %8.3f %8.3e % 8.3e %8.3e %n",level,getLevel("P",level),getLevel("T",level), getLevel("W",level), getLevel("O",level), getLevel("C",level));
    }
    
    /** Vertical interpolation at target output pressure points
     * @param VarId   Field ID ("T", "W", "O" or "C")
     * @param Pout    Array or output pressure points
     * @return Array of interpolated fields
     */
    public double[] VINT(String VarId,    // Wich variable to interpolate
                         double[] Pout){  // Output pressure points in ascending order
        
        int Nout = Pout.length;
        double[] VarOut = new double[Nout]; // Output variable
        
        // find the output pressure bound levels
        int j=0;
        boolean validInput= (VarId=="T") || (VarId=="W") || (VarId=="O") || (VarId=="C");
        if (!validInput) throw new IllegalArgumentException("Invalid VarId: "+VarId);
        
        for (int n=0; n<Nout;n++) {
            if (Pout[n]<this.P[0]) {
                // Output pressure lower than lowest input pressure -> extrapolate
                VarOut[n]=getLevel(VarId,0);
            }
            else if (Pout[n]>this.P[this.Nlev-1]) {
                // Output pressure lower than lowest input pressure -> extrapolate
                VarOut[n]=getLevel(VarId,this.Nlev-1);
            }
            else {
                while (this.P[j+1]<Pout[n]) j++;
                // the output pressure is between j and j+1
                
                if (VarId=="W" || VarId=="O") {
                    VarOut[n]= Math.exp( Math.log(getLevel(VarId,j))
                        + Math.log(Pout[n]/this.P[j]) * Math.log(getLevel(VarId,j+1)/ getLevel(VarId,j))
                        / Math.log(this.P[j+1]/this.P[j])
                    );
                }
                else if (VarId=="T" || VarId=="C") {
                    VarOut[n]=  getLevel(VarId,j) 
                        + Math.log(Pout[n]/this.P[j])*(getLevel(VarId,j+1)- getLevel(VarId,j))
                        / Math.log(this.P[j+1]/this.P[j]);
                }
                else {
                    // illegal VarId
                    VarOut[n]=airGlobals.MISSING;
                    logger.log(Level.WARNING,"Invalid input field:"+VarId); 
                }
            }
        }
        
        return VarOut;
    }
     
    /** Vertical integation of atmospheric conponents
     * <p>
     * The VarBot array values can be set to airGlobals.MISSING. In that case, 
     * the profile fields at level Sl will be used as surface values.
     * @param VarIds  List of Field IDs to integrate ("W", "O" or "C")
     * @param Pb      Bottom pressure bound
     * @param Pt      Top pressure bound
     * @param zs      Surface elevation
     * @param lat     Surface latitude
     * @param VarBot  Component mixing ratios at the bottom 
     * @return Array of integrated component (kg/m2)
     */
    public double[] TCOL(String[] VarIds,
                         double Pb,
                         double Pt,
                         double zs,
                         double lat,
                         double[] VarBot){  
                          
        // number of variables to integrate
        int NVar=VarIds.length;
        double[] TCOL = new double[NVar];
        
        // initialize
        double z0 = 0;  // Layer lower altitude
        double z1 = 0;  // Layer upper altitude. Used as an in/out argument in the P2HL function
        int i1 = 0;     // Layer upper level index. Used as an in/out argument in the P2HL function
        double meanVar = 0; // mean variable for a given layer
        
        // Find the index of the first level above the integration bottom layer
        // note that levels that are actually below the surface level will be ignored in the integration
        int ib=this.Sl;
        while (this.P[ib]>=Pb) {
            ib--;
        }
        
        // Find the index of the integration top layer
        int it=ib;
        while ( (this.P[Math.max(0,it-1)]>=Pt) && it>0 ) it--;

        // Check validity of botom layer quantities and replace if necessary
        for (int var=0; var<NVar; var++) {
            if (VarBot[var]==airGlobals.MISSING) VarBot[var]=this.getLevel(VarIds[var],ib); 
        }
        
        // Altitude of the first layer above the bottom layer
        z1 = P2HL(ib,zs,lat);
        // mean layer height
        double z =(zs+z1 )/2;
        
        // loop over variables to integrate
        for (int var=0; var<NVar; var++) {
            // Contribution of first layer above the bottom layer
            meanVar=(VarBot[var]+this.getLevel(VarIds[var],ib))/2;
            TCOL[var]=meanVar*(Pb-this.P[ib])*100/GRAV(z,lat);
        }
        
        // Loop over levels above ib
        for (int i=ib-1;i>=it;i--) {
            z0=z1;                  // copy previous height
            z1 = P2HL(i,zs,lat); // new upper level height
            z =(z0+z1 )/2;          // mean layer height
            
            // loop over variables to integrate
            for (int var=0; var<NVar; var++) {
                meanVar=(this.getLevel(VarIds[var],i+1)+this.getLevel(VarIds[var],i))/2;
                TCOL[var] += meanVar*(this.P[i+1]-this.P[i])*100/GRAV(z,lat);
            }
        }
        
        return TCOL;
    }
    
    /** Conversion from pressure to height levels
     * <p>
     * This method is optimized to be called multiple times with
     * decreasing pressure index (i.e. from surface to top of atm)
     * @param targetIndex   Target pressure index
     * @param zs            Surface altitude
     * @param lat           Surface latitude
     * @return              Level altitude (m)
     */
    public double P2HL(int targetIndex, // 
                       double zs,       // surface altitude
                       double lat){     // latitude
                       
        double P2HL=airGlobals.MISSING;
        double UpperVT = 0; 
        
        try {
        if (targetIndex<0 || targetIndex >=Nlev) throw new IllegalArgumentException("targetIndex is out of bounds:"+targetIndex);
        
        // Check if the algorithm shall restart from the surface
        if (targetIndex>this.gi) {
            // the target level is lower (i.e. higher pressure) than the last calculated one
            // it is either the first iteration, or the method is not called in decreasing pressure levels
            // System.out.println("height first iter " );
            
            // Calculate the mean virtual temperature of the first layer.
            // It is assumed W=[Sl] at the surface
            airParcel LowerParcel = new airParcel(this.Ps, this.Ts, this.W[Sl]);
            airParcel UpperParcel = new airParcel(this.P[Sl], this.T[Sl], this.W[Sl]);
            UpperVT = UpperParcel.VT();
            
            double VT=(LowerParcel.VT() + UpperVT )/2.0;
            
            // Contribution of the first layer above the surface
            P2HL=zs+(airGlobals.RL*VT)/GRAV(zs,lat)*Math.log(this.Ps/this.P[Sl] );
            
            // debug
            //System.out.format("P2HL first layer: P=[ %8.4f %8.4f] %n", this.Ps, this.P[Sl]);
            //System.out.format("P2HL first layer: VT=%8.4f %n", VT);
            //System.out.format("P2HL first layer: h=%8.4f %n", P2HL);
            
            // re-initialize gi and h
            this.gi = this.Sl;
            this.h = P2HL;
            
        }
        else {
            // Upper virtual temperature of the last iteration step
            airParcel UpperParcel = new airParcel(this.P[gi], this.T[gi], this.W[gi]);
            UpperVT = UpperParcel.VT();
            
            // get latest calculated height
            P2HL=this.h;
        }
        
        // now set the previous upper level as the lower level
        double LowerVT = UpperVT;
        
        // loop from previous pressure level to target level (altitude increase / index decreasing) 
        for (int i=this.gi-1; i>=targetIndex;i--) {
            
            
            // new upper parcel 
            airParcel UpperParcel = new airParcel(this.P[i],this.T[i], this.W[i]);
            UpperVT = UpperParcel.VT();
            
            // layer mean VT
            double VT=(LowerVT + UpperVT )/2.0;
            
            // apply altitude increment for the layer [i+1, i]
            P2HL += (airGlobals.RL*VT)/GRAV(P2HL,lat)*Math.log(this.P[i+1]/this.P[i] );
            //double dh=(airGlobals.RL*VT)/GRAV(P2HL,lat)*Math.log(this.P[i+1]/this.P[i] );
            //System.out.format("P2HL loop %3d %8.2f %8.2f %n", i, dh, P2HL);
            
            // save this VT for next iteration
            LowerVT = UpperVT;
        }
        
        // Save gravity integration index and calculated height
        this.gi = targetIndex;
        this.h = P2HL;
        
        // debug
        //System.out.format("h(%8.1f) = %8.5f %n", P[targetIndex],P2HL);
        
        return P2HL;
        }
        catch (IllegalArgumentException ex) {
                ex.printStackTrace();
                return airGlobals.MISSING;
        }
        
    }
    
    /** Level elevation 
     * <p>
     * This methods needs surface altitude and latitude information. If the 
     * profileGeometry is set, the values are taken from there. If not, null
     * values are used ba default.
     * @param level  Vertical level index
     * @return       elevation
     */
    public double elevation(int level) {
        if (this.geometry==null) {
            return P2HL(level,0.0, 0.0);
        } else {
            return P2HL(level,this.geometry.getLat(), this.geometry.getElev());
        }
    }
    
    /** Gravity acceleration 
     * @param z      Altitude
     * @param lat    Latitude
     * @return       g (m/s²)
     */
    public double GRAV(double z, double lat) {
        double C0 = 9.806160;
        double C1 = 3.085462e-6;
        double C2 = 2.27e-9;
        double C3 = 7.254e-13;
        double C4 = 1.0e-20;
        double C5 = 1.517e-19;
        double C6 = 6e-22;
        double C7 = 0.0026373;
        double C8 = 0.0000059;
        
        double cos= Math.cos(Math.toRadians(lat));
        
        double g = C0*(1 - C7*cos + C8*cos*cos) - (C1 +C2*cos)*z 
                 + (C3 + C4*cos)*z*z  - (C5 + C6*cos)*z*z*z;
        
        return g;
    }
    
    /** Check whether the P, T, W fields are valid
     * @param level    Vertical level
     * @return         Validity flag
     */
    public boolean isLevelValid(int level) {
        if (this.P[level]==airGlobals.MISSING || this.T[level]==airGlobals.MISSING || this.W[level]==airGlobals.MISSING) {
            return false;
        } else {
            return true;
        }
    }
    
    /** Check whether the P, T, W fields are valid up to a certain pressure level
     * @param topPressure  Top pressure (hPa)
     * @return             Validity flag
     */
    public boolean isFullUpTo(double topPressure) {
        int level=this.Sl;
        while (this.P[level]>=topPressure && level>=0) {
            if (!this.isLevelValid(level)) {
                return false;
            }
            level--;
        }
        return true;
    }
    
    /** Copy a vertical level from one profile to another
     * @param profile  Data to copy from
     * @param level    Vertical level index
     */
    private void copyLevel(airProfile profile, int level) {
        this.P[level] = profile.P[level];
        this.T[level] = profile.T[level];
        this.W[level] = profile.W[level];
        this.O[level] = profile.O[level];
        this.C[level] = profile.C[level];
    }
    
    /** Fill a profile missing levels using another profile
     * <p> 
     * The profile supplied as argument shall have no missing levels
     * <p>
     * The method used the gradient (for each variable) of the supplementary profile
     * to fill the gaps, while ensuring continuity with the original profile at both
     * ends of the gaps
     * @param supProfile  Supplementary profile used to fill the gaps
     * @return Filled profile 
     */
    public airProfile interpolate(airProfile supProfile) {
        
        // first duplicate original profile
        airProfile mergedProfile = (airProfile) this.clone();
        
        // check supProfile completeness
        if (supProfile.Mlev >0) {
            // return the original profile
            logger.log(Level.WARNING,"Profile in argument is not complete"); 
            return mergedProfile;
        }
        
        // If suppProfile surface level is below (Sl higher), reset the mergedProfile surface
        if (mergedProfile.Sl<supProfile.Sl) {
            mergedProfile.copyLevel(supProfile,supProfile.Sl);                
            mergedProfile.setSurface(supProfile.Ps, supProfile.Ts, supProfile.Sl);
        }
        
        // If the top level is missing, use supProfile
        if (!mergedProfile.isLevelValid(0)) mergedProfile.copyLevel(supProfile,0);
        
        // From merged profile first valid level, loop toward top of atmosphere
        int level=mergedProfile.Sl-1;
        while (level>0) {
            // move until the next missing level (nothing to do until then)
            while(level>0 && mergedProfile.isLevelValid(level)) level--;
            if (level==0) break;
                        
            // Now we have a gap. Find where is the next valid level
            int nextLevel = level-1;
            while(nextLevel>0 && !mergedProfile.isLevelValid(nextLevel)) nextLevel--;
                        
            // loop though the levels we have to fill
            for (int k=level; k>nextLevel; k--) {
                if (trace) logger.log(Level.FINE,String.format("Profile interpolation at level %d",k));
                
                mergedProfile.P[k]=supProfile.P[k];
                
                double factor = (level+1-k)/(double)(level+1-nextLevel);
                
                // supProfile sanity check. This should not happen unless profile is inconsistent
                if ( !supProfile.isLevelValid(k) || !supProfile.isLevelValid(level) ) {
                    logger.log(Level.WARNING,"Profile in argument has invalid levels");
                    continue;
                }
                
                // Fill data using two components:
                // The first component is obtained by applying the gradient from supProfile with respect
                // to the last valid level (level+1)
                // The second component is a "call-back" to the original profile ensure continuity with the next valid level
                if (supProfile.T[k]!=airGlobals.MISSING) 
                mergedProfile.T[k] = mergedProfile.T[level+1] + (supProfile.T[k]-supProfile.T[level+1])
                                    + factor*( (mergedProfile.T[nextLevel]-mergedProfile.T[level+1]) 
                                             - (supProfile.T[nextLevel]- supProfile.T[level+1]) );
               
                if (supProfile.W[k]!=airGlobals.MISSING)
                mergedProfile.W[k] = mergedProfile.W[level+1] + (supProfile.W[k]-supProfile.W[level+1])
                                    + factor*( (mergedProfile.W[nextLevel]-mergedProfile.W[level+1]) 
                                             - (supProfile.W[nextLevel]- supProfile.W[level+1]) );
               
                if (supProfile.O[k]!=airGlobals.MISSING)
                mergedProfile.O[k] = mergedProfile.O[level+1] + (supProfile.O[k]-supProfile.O[level+1])
                                    + factor*( (mergedProfile.O[nextLevel]-mergedProfile.O[level+1]) 
                                             - (supProfile.O[nextLevel]- supProfile.O[level+1]) );
                                             
                if (supProfile.C[k]!=airGlobals.MISSING)
                mergedProfile.C[k] = mergedProfile.C[level+1] + (supProfile.C[k]-supProfile.C[level+1])  
                                    + factor*( (mergedProfile.C[nextLevel]-mergedProfile.C[level+1]) 
                                             - (supProfile.C[nextLevel]- supProfile.C[level+1]) );
                
                // decrement the number of missing levels
                mergedProfile.Mlev--;
            }
            level=nextLevel;
        }
        return mergedProfile;
        
    }
    
    /** Write profile to ASCII file
     * <p>
     * See example files in atfu/data/profiles/
     * @param filename      Output file name
     */
    public void write(String filename) throws IOException {
        try {
            // open file in replace mode
            FileWriter fileWriter = new FileWriter(filename, false);
            PrintWriter printWriter = new PrintWriter(fileWriter);
            
            printWriter.printf("# Surface pressure%n");
            printWriter.printf("%8.3f%n",this.Ps);
            printWriter.printf("# Number of levels%n");
            printWriter.printf("%3d%n",this.Nlev);
            printWriter.printf("# Profile fields: P T W O C%n");
            for (int i=0;i<Nlev;i++) {
                printWriter.printf("%8.3f  %8.3f  %8.3e  %8.3e  %8.3e%n",
                          this.P[i], this.T[i], this.W[i], this.O[i], this.C[i]);
            }
            if(printWriter!= null) printWriter.close();
        }
        catch (Exception e) {
            System.out.println("Message catch: " + e.getMessage());
        }
        finally {
            // nothing to do
        }
    
    }
    
    
    /** 
     * Main function testing the class methods 
     */
    public static void main(String args[]) throws IOException {
        // force locale to avoid issues with reading / writing numbers 
        Locale l = new Locale("en", "GB");
        Locale.setDefault(l);
        
        // set logging
        String logFile = "logs/airProfile.log";
        airProfile.setLogging(true, logFile, "INFO");

        
        String input="data/profiles/profile_0423pwlr.txt";
        try {
            airProfile profile = new airProfile(input,0);
            
            System.out.format("  getLevel: P[60]= %6.2f%n", profile.getLevel("P",60));
        
            System.out.println("Testing P2HL (pressure to height conversion):");
            int level=0;
            double lat=5;
            double zs=0;
            System.out.format("  P2HL(level= %4d, Ts= %6.1f zs= %6.1f, lat= %6.2f) = %8.5f %n",level,profile.getTs(),zs,lat,profile.P2HL(level,zs,lat));
            
            System.out.println("Testing VINT (vertical interpolation):");
            double[] Pout = {200, 350, 550,1200};
            System.out.format("  P interp = %8.5f %8.5f %8.5f %8.5f%n",Pout[0],Pout[1],Pout[2], Pout[3]);
            double[] Tout = profile.VINT("T",Pout);
            System.out.format("  T interp = %8.5f %8.5f %8.5f %8.5f%n",Tout[0],Tout[1],Tout[2], Tout[3]);
            double[] Wout = profile.VINT("W",Pout);
            System.out.format("  W interp = %8.5f %8.5f %8.5f %8.5f%n",Wout[0],Wout[1],Wout[2], Wout[3]);
            
            System.out.println("Testing TCOL (vertical integration):");
            String[] VarIds = {"W","O","C"};
            double[] VarBot = {airGlobals.MISSING,airGlobals.MISSING,airGlobals.MISSING};
            double[] TCOL=profile.TCOL(VarIds,1015.0,350.0,0.0,45.0,VarBot);
            System.out.format("  TCOL = %8.5f %8.5f %8.5f %n",TCOL[0],TCOL[1],TCOL[2]);
            
            System.out.println("Testing GRAV (gravity acceleration):");
            double elev=0;
            System.out.format("  GRAV(elev= %6.1f, lat= %6.2f) = %8.5f %n",elev,lat,profile.GRAV(elev,lat));
            elev=10000;
            System.out.format("  GRAV(elev= %6.1f, lat= %6.2f) = %8.5f %n",elev,lat,profile.GRAV(elev,lat));
            
            System.out.println("");
            System.out.println("Adding geometry. Surface point is:");
            geoPos pos = new geoPos(45.0, 0.0, 0.0);
            pos.print();
            
            profile.addGeometry(pos, 45.0, 180.0);
            elev=11000;
            System.out.format("Sub-Profile Point at %7.2f m :%n",elev);
            profile.geometry.SPP(elev,0).print();
            
            
            System.out.println("Testing profile merge:");
            
            airProfile incompleteProfile = new airProfile("data/profiles/profile_0423tsv.txt",0);
            airProfile mergedProfile = incompleteProfile.interpolate(profile);
            mergedProfile.write("data/profiles/profile_0423merged.txt");
        
        }
        catch (IOException ex) {
                ex.printStackTrace();
        }
        
        LogManager.getLogManager().reset();  
    }
    
}
